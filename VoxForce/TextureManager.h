#pragma once

#include "StdAfx.h"

class CTextureManager
{
#pragma region _SINGLETON
private:
	// singleton: variables
	static CTextureManager* m_pInstance;
	static int m_nActiveInstances;
	
	// singleton: hide creationism
	CTextureManager(void);
	CTextureManager(const CTextureManager& mm);
	void operator=(const CTextureManager mm);
	~CTextureManager(void);
public:
	static CTextureManager* GetInstance();
	static void ReleaseInstance();
#pragma endregion
	
private:

public:

};

