#pragma once
#include "StdAfx.h"
#include "BaseObject.h"

class CCamera : public CBaseObject
{
	glm::mat4 m_ProjectionMatrix;
	glm::mat4 m_ViewMatrix;

	// viewport
	int m_nViewX;
	int m_nViewY;
	int m_nViewWidth;
	int m_nViewHeight;

	// view periferals
	float m_fFOV;
	float m_fAspect;

	// clip plain
	float m_fNear;
	float m_fFar;

public:
	
	virtual void Init() override;
	virtual void Update(float fDeltaTime) override;
	virtual void Shutdown() override;
	
	virtual ObjectType GetType() override { return CAMERA_OBJ;}

public:

	void BuildProjection(float fFOV, int nViewX, int nViewY, int nViewWidth, int nViewHeight, float fNear, float fFar);
	
	inline const glm::mat4& GetProjection() { return m_ProjectionMatrix;}
	inline const glm::mat4& GetView() 
	{ 
		m_ViewMatrix = glm::inverse(GetWorldMatrix());
		return m_ViewMatrix;
	}

	CCamera(void);
	~CCamera(void);
};

