
#include "StdAfx.h"
#include "Game.h"

#include "ObjectManager.h"
#include "MeshManager.h"
#include "RenderableObject.h"
#include "Camera.h"
#include "Chunk.h"

#include "Player.h"

CGame::CGame(void)
{
}

CGame::~CGame(void)
{
}

void CGame::Init(int width, int height, HWND hwnd, HINSTANCE hinstance)
{

	// get singletons
	m_pObjectManager = CObjectManager::GetInstance();
	m_pMeshManager =  CMeshManager::GetInstance();

	// initialize things
	m_pObjectManager->Init();

	// build camera
	CCamera* pCamera = new CCamera();
	pCamera->Init();
	m_pObjectManager->AddObject(pCamera);

	// init the rendering
	m_Renderer.SetCamera(pCamera);
	m_Renderer.Init(hwnd);
	m_Renderer.ResizeWindow((unsigned int)width, (unsigned int)height);
	m_Renderer.SetupScene();
	
	// create player
	CPlayer* pPlayer = new CPlayer();
	pPlayer->Init();
	pPlayer->SetShouldRender(false);
	m_pObjectManager->AddObject(pPlayer);

	// parent the camera to the player
	pCamera->SetParent(pPlayer);

	/*
	// build "floor"
	CBaseMesh* pFloor = m_pMeshManager->AddPlane(100);
	CRenderableObject* pFloorObj = new CRenderableObject();
	pFloorObj->SetMesh(pFloor);
	m_pObjectManager->AddObject((CBaseObject*)pFloorObj);
	*/

	// start the timer
	m_GameTimer.Start();
}

void CGame::Shutdown()
{
	// stop the timer
	m_GameTimer.Stop();

	// shutdown things
	m_pObjectManager->Shutdown();

	// release singletons
	CObjectManager::ReleaseInstance();
	CMeshManager::ReleaseInstance();
	m_pObjectManager = NULL;
	m_pMeshManager = NULL;

	// stop the renderer
	m_Renderer.Shutdown();
}



void CGame::Pause(bool bPause)
{
}

void CGame::ResizeGame(int nWidth, int nHeight)
{
	m_Renderer.ResizeWindow((unsigned int)nWidth, (unsigned int)nHeight);
}

void CGame::Stop()
{
	m_bRunning = false;
}

void CGame::Update()
{
	// update timer
	m_GameTimer.Update();

	// handle input

	// update states
	m_pObjectManager->Update((float)m_GameTimer.GetDeltaTime());

	// render
	m_Renderer.RenderBegin();
	m_pObjectManager->Render(&m_Renderer);
	m_Renderer.RenderEnd();
}
