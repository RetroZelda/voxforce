#pragma once

#include "StdAfx.h"

class CBaseObject;
class CRenderableObject;
class CRenderer;
class CChunkManager;
class CObjectManager
{
#pragma region _SINGLETON
private:
	// singleton: variables
	static CObjectManager* m_pInstance;
	static int m_nActiveInstances;
	
	// singleton: hide creationism
	CObjectManager(void);
	CObjectManager(const CObjectManager& mm);
	void operator=(const CObjectManager mm);
	~CObjectManager(void);
public:
	static CObjectManager* GetInstance();
	static void ReleaseInstance();
#pragma endregion
	
private:
	std::vector<CBaseObject*> m_pObjectList;

	void EachObjectLoop(void (*pFunc)(CBaseObject*));
	void (*m_pRenderCallback)(CRenderableObject*, CRenderer*);

	// Vexel Rendering stuff
	CChunkManager* m_pChunkManager;

public:
	
	void SetRenderCallback(void (*pRenderFunc)(CRenderableObject*, CRenderer*));

	void AddObject(CBaseObject* pObject);
	void RemoveObject(CBaseObject* pObject);

	void Init();
	void Shutdown();
	void Update(float fDeltaTime);
	void Render(CRenderer* pRenderer);

};

