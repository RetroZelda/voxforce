#pragma once

#include "StdAfx.h"

class CObjectManager;
class CMeshManager;
class CShaderManager;
class CBaseMesh;
class CBaseObject;
class CRenderableObject;
class CCamera;
class CRenderer
{
	// window-related
	HWND		m_hWnd;
	UINT		m_nWidth;
	UINT		m_nHeight;

	// GLEW-related
	HGLRC	m_hRenderContext;
	HDC		m_hDeviceContext;
		
	// rendering
	float m_ClearColor[4];
	glm::mat4 m_ViewProj;

	// cameras
	CCamera* m_pCamera;

	// uniforms
	GLuint m_MVP;
	GLuint m_Model;
	GLuint m_View;
	GLuint m_Projection;
	GLuint m_ModelView;
	GLuint m_NormalMat;
	GLuint m_LightPos;

	// for actual rendering
	GLuint m_VertexArrayID;

	// misc variables
	bool m_bInitialized;

	// Misc Managers
	CObjectManager* m_pObjectManager;
	CMeshManager* m_pMeshManager;
	CShaderManager* m_pShaderManager;

	void BuildProjection();
	void Bind();
	void Unbind();
		
	static void MeshCallback(CRenderableObject* pObj, CRenderer* pRenderer);
public:
	CRenderer(void);
	~CRenderer(void);

	void SetCamera(CCamera* pCam) { m_pCamera = pCam;}
	CCamera* GetCamera() { return m_pCamera;}

	bool Init(HWND hWnd);
	void SetupScene();
	void ResizeWindow(UINT w, UINT h);
	void RenderBegin();
	void RenderEnd();
	void Shutdown();
};