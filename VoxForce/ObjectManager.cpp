#include "StdAfx.h"
#include "ObjectManager.h"
#include "BaseObject.h"
#include "ChunkManager.h"
#include "RenderableObject.h"

// ensure it starts null
CObjectManager* CObjectManager::m_pInstance = NULL; 
int CObjectManager::m_nActiveInstances = 0;

CObjectManager::CObjectManager(void)
{
}

CObjectManager::~CObjectManager(void)
{
}

CObjectManager::CObjectManager(const CObjectManager& mm)
{
}

void CObjectManager::operator=(const CObjectManager mm)
{

}

CObjectManager* CObjectManager::GetInstance()
{
	if(m_pInstance == NULL)
	{
		assert(m_nActiveInstances == 0 && "m_nActiveInstances out of sync");
		m_pInstance = new CObjectManager();
	}

	m_nActiveInstances++;
	return m_pInstance;
}

void CObjectManager::ReleaseInstance()
{
	m_nActiveInstances--;

	// delete the instance if no one has a handle
	if(m_nActiveInstances == 0)
	{
		delete m_pInstance;
		m_pInstance = NULL;
	}
}

// loop through each object and call the passed in function
void CObjectManager::EachObjectLoop(void (*pFunc)(CBaseObject*))
{
	assert(pFunc != NULL && "Cannot call a NULL function!");
	for(unsigned int objectIndex = 0; objectIndex < m_pObjectList.size(); ++objectIndex)
	{
		CBaseObject* pCurObj = m_pObjectList[objectIndex];
		pFunc(pCurObj);
	}
}

void CObjectManager::AddObject(CBaseObject* pObject)
{
	assert(pObject != NULL && "Cannot push a NULL Object!");
	m_pObjectList.push_back(pObject);
}

void CObjectManager::RemoveObject(CBaseObject* pObject)
{
	assert(pObject != NULL && "Cannot remove a NULL Object!");
	
}

void CObjectManager::Init()
{
	m_pChunkManager = CChunkManager::GetInstance();
	m_pChunkManager->Init(10, 10, 10);
}

void CObjectManager::Shutdown()
{
	m_pChunkManager->Shutdown();
	CChunkManager::ReleaseInstance();
	m_pChunkManager = NULL;
}

void CObjectManager::Update(float fDeltaTime)
{
	// update our voxel data
	m_pChunkManager->Update(fDeltaTime);

	for(unsigned int objectIndex = 0; objectIndex < m_pObjectList.size(); ++objectIndex)
	{
		CBaseObject* pCurObj = m_pObjectList[objectIndex];
		pCurObj->Update(fDeltaTime);
	}
}

void CObjectManager::Render(CRenderer* pRenderer)
{
	// render our voxels
	m_pChunkManager->Render(pRenderer);

	// the callbacks neesd to be set
	assert(m_pRenderCallback != NULL && "The Mesh Callback needs to be set!");

	for(unsigned int objectIndex = 0; objectIndex < m_pObjectList.size(); ++objectIndex)
	{
		CBaseObject* pCurObj = m_pObjectList[objectIndex];
		switch(pCurObj->GetType())
		{
		case CBaseObject::CHUNK_OBJ:
		case CBaseObject::PLAYER_OBJ:
		case CBaseObject::RENDERABLE_OBJ:
			{
				if(((CRenderableObject*)pCurObj)->ShouldRender() == true)
				{
					m_pRenderCallback((CRenderableObject*)pCurObj, pRenderer);
				}
			} break;
		};
	}
}
void CObjectManager::SetRenderCallback(void (*pRenderFunc)(CRenderableObject*, CRenderer*)) 
{
	// for now, voxels share same render callback
	m_pChunkManager->SetRenderCallback(pRenderFunc);
	m_pRenderCallback = pRenderFunc;
}

