#include "StdAfx.h"
#include "ChunkManager.h"
#include "Renderer.h"
#include "Chunk.h"
#include "Block.h"
#include "Camera.h"

// ensure it starts null
CChunkManager* CChunkManager::m_pInstance = NULL; 
int CChunkManager::m_nActiveInstances = 0;

CChunkManager::CChunkManager(void)
{
	m_pCamera = NULL;
}

CChunkManager::~CChunkManager(void)
{

}

CChunkManager::CChunkManager(const CChunkManager& mm)
{
}

void CChunkManager::operator=(const CChunkManager mm)
{

}

CChunkManager* CChunkManager::GetInstance()
{
	if(m_pInstance == NULL)
	{
		assert(m_nActiveInstances == 0 && "m_nActiveInstances out of sync");
		m_pInstance = new CChunkManager();
	}
	m_nActiveInstances++;
	return m_pInstance;
}

void CChunkManager::ReleaseInstance()
{
	m_nActiveInstances--;

	// delete the instance if no one has a handle
	if(m_nActiveInstances == 0)
	{
		delete m_pInstance;
		m_pInstance = NULL;
	}
}

void CChunkManager::Init(int nWidth, int nHeight, int nDepth)
{
	printf("Initializing %d x %d x %d chunks!\n", nWidth, nHeight, nDepth);
	m_nWidth = nWidth;
	m_nHeight = nHeight;
	m_nDepth = nDepth;
	
	// Create the X block
	m_WorldChunks = new CChunk**[nWidth];
	for(int i = 0; i < nWidth; i++)
	{
		// crete the Y blocks
		m_WorldChunks[i] = new CChunk*[nHeight];

		for(int j = 0; j < nHeight; j++)
		{
			// create the Z blocks
			m_WorldChunks[i][j] = new CChunk[nDepth];
			for(int k = 0; k < nDepth; k++)
			{
				// init the chunk and push it back so we can init them
				m_WorldChunks[i][j][k].SetMyPosition(i, j, k);
				m_WorldChunks[i][j][k].Init();
				m_InitChunks.push_back(&m_WorldChunks[i][j][k]);
			}
		}
	}
	printf("Initialized All Chunks!\n");
}

void CChunkManager::Update(float fDeltaTime)
{
	/*
	static bool bPrev = false;
	bool bCur = GetAsyncKeyState(VK_SPACE);
	if(bCur == true)// && bPrev == false)
	{
		// get a random chunk and a random block to deactivate
		int randPos = rand() % m_RenderChunks.size();
		int randX = rand() % CChunk::CHUNK_SIZE;
		int randY = rand() % CChunk::CHUNK_SIZE;
		int randZ = rand() % CChunk::CHUNK_SIZE;

		// make the block inactive
		CChunk *pChunk = m_RenderChunks.at(randPos);
		pChunk->DisableBlock(randX, randY, randZ);
	}
	bPrev = bCur;
	*/
	// for now, erase blocks that the camera goes into
	if(m_pCamera != NULL)
	{
		glm::vec3 camPos = m_pCamera->GetWorldPosition();
		glm::vec3 camForeward = m_pCamera->GetWorldForward();
		glm::vec3 selectedVoxel;

		static bool bPrev = false;
		bool bCur = GetAsyncKeyState(VK_SPACE) != 0;
				
		if(bCur == true && bPrev == false)
		{
			float interationJump = CBlock::BLOCK_RENDER_SIZE;
			float maxIterations = 10.0f * interationJump;
			for(float voxelForeward = 1.0f; voxelForeward < maxIterations; voxelForeward += interationJump)
			{
				selectedVoxel = camPos + (camForeward * -voxelForeward);

				int nBlockIndex[3];
				CChunk* pChunk = GetBlock(selectedVoxel, nBlockIndex);
				if(pChunk != NULL)
				{
					CBlock &pBlock = pChunk->GetBlock(nBlockIndex[0], nBlockIndex[1], nBlockIndex[2]);
					bool dis = pBlock.IsActive();
					//bool dis = pChunk->DisableBlock(nBlockIndex[0], nBlockIndex[1], nBlockIndex[2]);
					if(dis == true)
					{
						pChunk->DisableBlock(nBlockIndex[0], nBlockIndex[1], nBlockIndex[2]);
						break;
					}

					/*
					printf("Disabled Block(%d, %d, %d) in Chunk(%d, %d, %d)\n", nBlockIndex[0], nBlockIndex[1], nBlockIndex[2],
					pChunk->GetPosX(), pChunk->GetPosY(), pChunk->GetPosZ());
					*/
				}
			}
		}
		bPrev = bCur;
	}

	InitChunkList();
	BuildChunkList();
}

void CChunkManager::BuildChunkList()
{
	unsigned int nChunksBuilt = 0;
	vector<CChunk*>::iterator buildIter = m_BuildChunks.begin();
	while(m_BuildChunks.size() > 0 && nChunksBuilt < CHUNKS_TO_LOAD_PER_FRAME)
	{
		// remove the chunk from the list
		CChunk* pChunk = (*buildIter);
		m_BuildChunks.erase(buildIter);

		// build it
		pChunk->BuildMesh();

		// should be render safe
		//m_RenderChunks.push_back(pChunk);

		// move on
		buildIter = m_BuildChunks.begin();
		nChunksBuilt++;
	}
}

void CChunkManager::InitChunkList()
{
	unsigned int nChunkCount = 0;
	vector<CChunk*>::iterator chunkIter = m_InitChunks.begin();
	while(m_InitChunks.size() > 0 && nChunkCount < CHUNKS_TO_LOAD_PER_FRAME)
	{
		// remove the chunk from the list
		CChunk* pChunk = (*chunkIter);
		m_InitChunks.erase(chunkIter);

		// init everything else
		SetAdjacentChunks(pChunk);

		// ready to build
		m_BuildChunks.push_back(pChunk);
		m_RenderChunks.push_back(pChunk);

		// move on
		chunkIter = m_InitChunks.begin();
		nChunkCount++;
	}
}


void CChunkManager::Shutdown()
{
	// Delete the blocks
	for (int i = 0; i < m_nWidth; ++i)
	{
		for (int j = 0; j < m_nHeight; ++j)
		{
			for(int k = 0; k < m_nDepth; ++k)
			{
				// shutdown each chunk
				m_WorldChunks[i][j][k].Shutdown();
			}
			// delete the Z blocks
			delete [] m_WorldChunks[i][j];
		}

		// delete the Y blocks
		delete [] m_WorldChunks[i];
	}
	// delete hte X blocks
	delete [] m_WorldChunks;
}

void CChunkManager::Render(CRenderer* pRenderer)
{
	// the callbacks neesd to be set
	assert(m_pRenderCallback != NULL && "The Mesh Callback needs to be set!");

	// ensure we have hte camera
	if(m_pCamera == NULL)
	{
		m_pCamera = pRenderer->GetCamera();
	}

	// render each chunk that is set to be rendered
	vector<CChunk*>::iterator chunkIter = m_RenderChunks.begin();
	int iterCount = 0;
	for(; chunkIter != m_RenderChunks.end(); ++chunkIter, ++iterCount)
	{
		CChunk* pCurChunk = *chunkIter;

		// move to build list if needs rebuilt
		if(pCurChunk->GetStatus() == CChunk::CHUNK_NEED_REBUILT)
		{
			pCurChunk->SetStatus(CChunk::CHUNK_REBUILD_QUEUED);
			m_BuildChunks.push_back(pCurChunk);
		}

		// render if we have chunks TO render
		if(pCurChunk->ShouldRender())
		{
			m_pRenderCallback((CRenderableObject*)pCurChunk, pRenderer);
		}
	}
}


void CChunkManager::SetAdjacentChunks(CChunk* pChunk)
{
	// our position
	int i = pChunk->GetPosX();
	int j = pChunk->GetPosY();
	int k = pChunk->GetPosZ();

	// our neighbors
	CChunk* pLeft = NULL;
	CChunk* pRight = NULL; 
	CChunk* pFront = NULL;
	CChunk* pBack = NULL;
	CChunk* pTop = NULL;
	CChunk* pBottom = NULL;

	// get the adjacencies
	// get the left
	if(i == 0)
	{
		pLeft = NULL;
	}
	else
	{
		pLeft = &m_WorldChunks[i - 1][j][k];
	}

	// get the right
	if(i == m_nWidth - 1)
	{
		pRight = NULL; 
	}
	else
	{
		pRight = &m_WorldChunks[i + 1][j][k];
	}
				
	// get the bottom
	if(j == 0)
	{
		pBottom = NULL; 
	}
	else
	{
		pBottom = &m_WorldChunks[i][j - 1][k];
	}

	// get the top
	if(j == m_nHeight - 1)
	{
		pTop = NULL;
	}
	else
	{
		pTop = &m_WorldChunks[i][j + 1][k];
	}

	// get the front
	if(k == 0)
	{
		pFront = NULL;
	}
	else
	{
		pFront = &m_WorldChunks[i][j][k - 1];
	}

	// get the back
	if(k == m_nDepth - 1)
	{
		pBack = NULL; 
	}
	else
	{
		pBack = &m_WorldChunks[i][j][k + 1];
	}

	// set them
	pChunk->SetAdjacent(pLeft, pRight, pFront, pBack, pTop, pBottom);
}

// Get a chunk with a World position
CChunk* CChunkManager::GetChunk(glm::vec3 v3Pos)
{
	int nBlockSize = (int)((float)CChunk::CHUNK_SIZE * CBlock::BLOCK_RENDER_SIZE);

	// each value must be > 0
	if(v3Pos.x < 0.0f || v3Pos.y < 0.0f || v3Pos.z < 0.0f)
	{
		return NULL;
	}

	// each value must be < the world size
	glm::vec3 worldSize = glm::vec3(m_nWidth * nBlockSize, m_nHeight * nBlockSize, m_nDepth * nBlockSize);
	if(v3Pos.x > worldSize.x || v3Pos.y > worldSize.y || v3Pos.z > worldSize.z)
	{
		return NULL;
	}

	// index arrays
	int blockWorldIndex[3];
	int chunkIndex[3];

	// the blocks world index
	blockWorldIndex[0] = (int)v3Pos.x;
	blockWorldIndex[1] = (int)v3Pos.y;
	blockWorldIndex[2] = (int)v3Pos.z;

	// the chunk index for the block
	chunkIndex[0] = blockWorldIndex[0] / nBlockSize;
	chunkIndex[1] = blockWorldIndex[1] / nBlockSize;
	chunkIndex[2] = blockWorldIndex[2] / nBlockSize;


	return &m_WorldChunks[chunkIndex[0]][chunkIndex[1]][chunkIndex[2]];
}

CChunk* CChunkManager::GetBlock(glm::vec3 v3Pos, int* pIndexOut)
{
	// get our chunk
	CChunk* pChunk = GetChunk(v3Pos);
	if(pChunk == NULL)
	{
		return NULL;
	}

	int nBlockSize = (int)((float)CChunk::CHUNK_SIZE * CBlock::BLOCK_RENDER_SIZE);
	pIndexOut[0] = (int)v3Pos.x % nBlockSize;
	pIndexOut[1] = (int)v3Pos.y % nBlockSize;
	pIndexOut[2] = (int)v3Pos.z % nBlockSize;

	return pChunk;
}

// Get a block with a World position
CBlock* CChunkManager::GetBlock(glm::vec3 v3Pos)
{
	// get our chunk
	CChunk* pChunk = GetChunk(v3Pos);
	if(pChunk == NULL)
	{
		return NULL;
	}

	return GetBlock(v3Pos, pChunk);
}

CBlock*  CChunkManager::GetBlock(glm::vec3 v3Pos, CChunk*pChunk)
{
	// the block index in the chunk
	int nBlockSize = (int)((float)CChunk::CHUNK_SIZE * CBlock::BLOCK_RENDER_SIZE);
	int blockIndex[3];
	blockIndex[0] = pChunk->GetPosX() % nBlockSize;
	blockIndex[1] = pChunk->GetPosY() % nBlockSize;
	blockIndex[2] = pChunk->GetPosZ() % nBlockSize;

	return &pChunk->GetBlock(blockIndex[0], blockIndex[1], blockIndex[2]);
}