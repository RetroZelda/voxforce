#include "StdAfx.h"

#include "Camera.h"

CCamera::CCamera(void)
{
}


CCamera::~CCamera(void)
{
}


void CCamera::BuildProjection(float fFOV, int nViewX, int nViewY, 
/**/int nViewWidth, int nViewHeight, float fNear, float fFar)
{

	// set the variables
	m_fFOV = fFOV;
	m_nViewX = nViewX;
	m_nViewY = nViewY;
	m_nViewWidth = nViewWidth;
	m_nViewHeight = nViewHeight;
	m_fNear = fNear;
	m_fFar = fFar;

	// set the view and the projection
	m_fAspect = (float)m_nViewWidth / (float)m_nViewHeight;
	glViewport(m_nViewX, m_nViewY, m_nViewWidth, m_nViewHeight);
	m_ProjectionMatrix = glm::perspective(fFOV, m_fAspect, 0.001f, 1000.0f);
}


void CCamera::Init()
{
	CBaseObject::Init();
}

void CCamera::Update(float fDeltaTime)
{
	CBaseObject::Update(fDeltaTime);
}

void CCamera::Shutdown()
{
	CBaseObject::Shutdown();
}