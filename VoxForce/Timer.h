#pragma once
#include "StdAfx.h"
#define LEAN_AND_MEAN
#include <WinDef.h>


class CTimer
{
private:
	double		m_dElapsedTime; // stored time
	double		m_dDeltaTime;
	bool		m_bIsRunning;
	LONGLONG	m_llStartTick;
	LONGLONG	m_llFrequency; // how many ticks per second
	
	// for fps
	int			m_nFrameCount;
	double		m_dFPSTimeStamp;
	int			m_nFPS;

public:
	CTimer(void);

	//stop watch functions
	void Start(void);
	void Stop(void);
	void Update(void);
	void Reset(void);

	// modifiers
	void Pause(bool bPause);

	// accessors
	double GetElapsedTime(void);
	double GetDeltaTime(void);
	int  FPS(void);
};