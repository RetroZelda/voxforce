
#include "StdAfx.h"
#include "MeshManager.h"
#include "BaseMesh.h"
#include "Renderer.h"

// ensure it starts null
CMeshManager* CMeshManager::m_pInstance = NULL; 
int CMeshManager::m_nActiveInstances = 0;

CMeshManager::CMeshManager(void)
{
}

CMeshManager::~CMeshManager(void)
{
	m_pInstance->Clear();
}

CMeshManager::CMeshManager(const CMeshManager& mm)
{
}

void CMeshManager::operator=(const CMeshManager mm)
{

}

CMeshManager* CMeshManager::GetInstance()
{
	if(m_pInstance == NULL)
	{
		assert(m_nActiveInstances == 0 && "m_nActiveInstances out of sync");
		m_pInstance = new CMeshManager();
	}
	m_nActiveInstances++;
	return m_pInstance;
}

void CMeshManager::ReleaseInstance()
{
	m_nActiveInstances--;

	// delete the instance if no one has a handle
	if(m_nActiveInstances == 0)
	{
		// clear
		m_pInstance->Clear();

		delete m_pInstance;
		m_pInstance = NULL;
	}
}

/*
void CMeshManager::Render(CRenderer* pRenderer)
{
	// call the callback on each mesh
	std::vector<CBaseMesh*>::iterator meshIter = m_pMeshList.begin();
	for(; meshIter != m_pMeshList.end(); ++meshIter)
	{
		CBaseMesh::MeshType meshType = (*meshIter)->GetType();
		switch(meshType)
		{
		case CBaseMesh::MESH_BASE:
			{
				CBaseMesh* pMesh = *meshIter;
			} break;
		default:
			{
				assert(false && "Bad Mesh Type!");
			} break;
		}
	}
}
*/

void CMeshManager::Clear()
{
	int nSize = m_pMeshList.size();
	while(nSize > 0)
	{
		delete m_pMeshList[nSize - 1];
		m_pMeshList.pop_back();
		nSize = m_pMeshList.size();
	}
}

CBaseMesh* CMeshManager::AddCube()
{
	float verts[] =
	{
		  /* Position */	 /* UV */	/*	  /* Normal */			   /* Color */								 /**/
	/*    X     Y     Z      U     V    /*     X     Y     Z       R     G     B     A   */						 /**/
																												 
		// front																								 
		-1.0, -1.0,  1.0,	0.0f, 0.0f,		0.0f, 1.0f, 0.0f,	1.0f, 0.0f, 0.0f, 1.0f, // bottom left	// 0	 
		 1.0, -1.0,  1.0,	0.0f, 0.0f,		0.0f, 1.0f, 0.0f,	0.0f, 1.0f, 0.0f, 1.0f, // bottom right	// 1	 
																										 		 
		 1.0,  1.0,  1.0,	0.0f, 0.0f,		0.0f, 1.0f, 0.0f,	0.0f, 0.0f, 1.0f, 1.0f, // top right	// 2	 
		-1.0,  1.0,  1.0,	0.0f, 0.0f,		0.0f, 1.0f, 0.0f,	0.0f, 1.0f, 1.0f, 1.0f, // top left		// 3	 
																												 
		// back																									 
		-1.0, -1.0, -1.0,	0.0f, 0.0f,		0.0f, 1.0f, 0.0f,	1.0f, 0.0f, 0.0f, 1.0f, // bottom left	// 4	 
		 1.0, -1.0, -1.0,	0.0f, 0.0f,		0.0f, 1.0f, 0.0f,	0.0f, 1.0f, 0.0f, 1.0f, // bottom right	// 5	 
																												 
		 1.0,  1.0, -1.0,	0.0f, 0.0f,		0.0f, 1.0f, 0.0f,	0.0f, 0.0f, 1.0f, 1.0f, // top right	// 6	 
		-1.0,  1.0, -1.0,	0.0f, 0.0f,		0.0f, 1.0f, 0.0f,	0.0f, 1.0f, 1.0f, 1.0f, // top left		// 7	 
	};
	Vertex cubeVerts[8];
	memcpy(&cubeVerts[0], &verts[0], sizeof(Vertex) * 8);  

	GLushort cubeIndices[36] = 
	{
		// front
		0, 1, 2,
		0, 2, 3,
		// top
		3, 2, 6,
		6, 7, 3,
		// back
		7, 6, 5,
		5, 4, 7,
		// bottom
		4, 5, 1,
		1, 0, 4,
		// left
		4, 0, 3,
		3, 7, 4,
		// right
		1, 5, 6,
		6, 2, 1,
	};      
	
	CBaseMesh* pMesh = new CBaseMesh();

	// add each vert
	for(int vertIndex = 0; vertIndex < 8; ++vertIndex)
	{
		pMesh->AddVert(cubeVerts[vertIndex]);
	}

	// add each triangle
	for(int triIndex = 0; triIndex < 36; triIndex += 3)
	{
		Triangle tri;
		memcpy(&tri, &cubeIndices[triIndex], sizeof(Triangle));
		pMesh->AddTriangle(tri);
	}

	pMesh->SendToGPU();
	m_pMeshList.push_back(pMesh);
	
	return pMesh;
}

CBaseMesh* CMeshManager::AddPlane(float fSize)
{
	float verts[] =
	{
		  /* Position */	     /* UV */		  /* Normal */			   /* Color */								 /**/
	/*    X     Y     Z          U     V         X     Y     Z       R     G     B     A   */						 /**/																							 
		-fSize, 0.0,  fSize,	0.0f, 0.0f,		0.0f, 1.0f, 0.0f,	1.0f, 0.0f, 0.0f, 1.0f, 	 
		 fSize, 0.0,  fSize,	0.0f, 0.0f,		0.0f, 1.0f, 0.0f,	0.0f, 1.0f, 0.0f, 1.0f, 		 
		-fSize, 0.0, -fSize,	0.0f, 0.0f,		0.0f, 1.0f, 0.0f,	0.0f, 0.0f, 1.0f, 1.0f, 	 
		 fSize, 0.0, -fSize,	0.0f, 0.0f,		0.0f, 1.0f, 0.0f,	0.0f, 1.0f, 1.0f, 1.0f, 	 
	};
	Vertex cubeVerts[8];
	memcpy(&cubeVerts[0], &verts[0], sizeof(Vertex) * 8);  

	GLushort cubeIndices[6] = 
	{
		0, 1, 2,
		2, 1, 3,
	};      
	
	CBaseMesh* pMesh = new CBaseMesh();

	// add each vert
	for(int vertIndex = 0; vertIndex < 8; ++vertIndex)
	{
		pMesh->AddVert(cubeVerts[vertIndex]);
	}

	// add each triangle
	for(int triIndex = 0; triIndex < 36; triIndex += 3)
	{
		Triangle tri;
		memcpy(&tri, &cubeIndices[triIndex], sizeof(Triangle));
		pMesh->AddTriangle(tri);
	}

	pMesh->SendToGPU();

	m_pMeshList.push_back(pMesh);
	
	return pMesh;
}