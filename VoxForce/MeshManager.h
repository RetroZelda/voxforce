#pragma once

#include "StdAfx.h"
#include "Renderer.h"
#include "BaseMesh.h"

class CRenderer;

class CMeshManager
{
#pragma region _SINGLETON
private:
	// singleton: variables
	static CMeshManager* m_pInstance;
	static int m_nActiveInstances;
	
	// singleton: hide creationism
	CMeshManager(void);
	CMeshManager(const CMeshManager& mm);
	void operator=(const CMeshManager mm);
	~CMeshManager(void);
public:
	static CMeshManager* GetInstance();
	static void ReleaseInstance();
#pragma endregion
	
private:
	std::vector<CBaseMesh*> m_pMeshList;
public:

	//void Render(CRenderer* pRenderer);
	void Clear();
	
	CBaseMesh* AddCube();
	CBaseMesh* AddPlane(float fSize);
};

