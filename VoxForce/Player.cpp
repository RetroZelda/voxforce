#include "StdAfx.h"
#include "Player.h"
#include "ChunkManager.h"

CPlayer::CPlayer(void)
{
	m_pChunkManager = NULL;
}


CPlayer::~CPlayer(void)
{
}

void CPlayer::Init()
{
	CRenderableObject::Init();
	// get singletons
	m_pChunkManager = CChunkManager::GetInstance();
}

void CPlayer::Update(float fDeltaTime)
{
	CRenderableObject::Update(fDeltaTime);

	const float fSpeed = 0.1f;
	const float fRotate = 5.0f;
	float fZ = 0.0f;
	float fX = 0.0f;
	float fY = 0.0f;
	float fYaw = 0.0f;
	float fPitch = 0.0f;
	if(GetAsyncKeyState('W'))
	{
		fZ -= fSpeed;
	}
	if(GetAsyncKeyState('S'))
	{
		fZ += fSpeed;
	}
	if(GetAsyncKeyState('A'))
	{
		fYaw += fRotate;
	}
	if(GetAsyncKeyState('D'))
	{
		fYaw -= fRotate;
	}
	if(GetAsyncKeyState('R'))
	{
		fPitch += fRotate;
	}
	if(GetAsyncKeyState('F'))
	{
		fPitch -= fRotate;
	}
	if(GetAsyncKeyState('Q'))
	{
		fX -= fSpeed;
	}
	if(GetAsyncKeyState('E'))
	{
		fX += fSpeed;
	}
	if(GetAsyncKeyState(VK_ADD))
	{
		fY += fSpeed;
	}
	if(GetAsyncKeyState(VK_SUBTRACT))
	{
		fY -= fSpeed;
	}
	
	Translate(fX, fY, fZ);

	glm::vec3 right = GetRight();
	glm::vec3 Forward = GetForward();
	glm::vec3 Up = GetUp();
	
	Rotate(vec3(0.0, 1.0, 0.0), fYaw);
	Rotate(vec3(1.0, 0.0, 0.0), fPitch);

	//printf("player Pos(%f. %f. %f)\n", GetWorldPosition().x, GetWorldPosition().y, GetWorldPosition().z);
}

void CPlayer::Render(CRenderer* pRenderer)
{
	CRenderableObject::Render(pRenderer);
}

void CPlayer::Shutdown()
{
	CRenderableObject::Shutdown();

	// release singletons
	CChunkManager::ReleaseInstance();
	m_pChunkManager = NULL;
}
