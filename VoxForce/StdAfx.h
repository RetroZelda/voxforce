#pragma once
//#include <Windows.h>
//#include <WinDef.h>

// openGL
#include <GL/glew.h>
#include <GL/wglew.h>

// STL
#include <vector>
#include <string>
#include <map>
#include <fstream>
#include <stdio.h>
#include <assert.h>

// math
#include "Math\RetroMath.h"

// namespaces
using namespace std;

// ensure bools are 0 and 1
/*
#define true 1
#define false 0
*/

/* NOTE: Linked in VS2010 Linker settings
#pragma comment(lib, "glew32.lib")  
#pragma comment(lib, "opengl32.lib") 
*/