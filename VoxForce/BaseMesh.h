#pragma once

#include "StdAfx.h"

// helper macros
#define BUFFER_OFFSET(i) ((char *)NULL + (i)) // offset in structures for VBOs and IBOs

#define VERT_POS		1 << 0
#define VERT_UV			1 << 1
#define VERT_NORMAL		1 << 2
#define VERT_TANGENT	1 << 3
#define VERT_COLOR		1 << 4

#define VERTEX_INIT	1 << 0
#define INDEX_INIT	1 << 1

struct Vertex
{
	vec3 pos;
	vec2 uv;
	vec3 normal;
	vec4 color;

	static Vertex MakeVertex(vec3 p, vec2 t, vec3 n, vec4 c)
	{
		Vertex vert;
		vert.pos = p;
		vert.uv = t;
		vert.normal = n;
		vert.color = c;

		return vert;
	}
	
	static void EnableAttribArray()
	{
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);
		glEnableVertexAttribArray(0);

		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), BUFFER_OFFSET(12));
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), BUFFER_OFFSET(20));
		glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), BUFFER_OFFSET(32));
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), BUFFER_OFFSET(0));
	}
	static void DisableAttribArray()
	{
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
		glDisableVertexAttribArray(3);
		glDisableVertexAttribArray(0);
	}

	static void SetVertexPointers()
	{
		// NOTE: Set vertex last as an optimization
		glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex), BUFFER_OFFSET(12));
		glNormalPointer(GL_FLOAT, sizeof(Vertex), BUFFER_OFFSET(20));
		glColorPointer(4, GL_FLOAT, sizeof(Vertex), BUFFER_OFFSET(32));
		glVertexPointer(3, GL_FLOAT, sizeof(Vertex), BUFFER_OFFSET(0));
	}
	
	static void EnableClientState()
	{
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);
		glEnableClientState(GL_NORMAL_ARRAY);
		glEnableClientState(GL_VERTEX_ARRAY);
	}

	static void DisableClientState()
	{
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);
	}
};

struct Triangle
{
	GLushort _0;
	GLushort _1;
	GLushort _2;

	static void ReverseWinding(Triangle& tri)
	{
		tri._1 ^= tri._2;
		tri._2 ^= tri._1;
		tri._1 ^= tri._2;
	}

	static glm::vec3 CalculateNormal(Triangle& tri, Vertex *verts)
	{
		// 2 sides
		glm::vec3 v01 = glm::normalize(verts[tri._0].pos - verts[tri._1].pos);
		glm::vec3 v02 = glm::normalize(verts[tri._0].pos - verts[tri._2].pos);

		// calculate the normal
		glm::vec3 normal = glm::cross(v01, v02);

	}
};

class CBaseMesh
{
	friend class CMeshManager;
	friend class CRenderer;
	friend class CChunk;
public:
	enum MeshType { MESH_BASE };
private:
	MeshType m_Type;

protected:
	unsigned int m_VertType;
	GLuint m_VertexBuffer;
	GLuint m_IndexBuffer;

	std::vector<Vertex> m_Verts;
	std::vector<Triangle> m_Triangles;

	byte m_InitFlags;

	inline void SetType(MeshType type) { m_Type = type;}

public:
	CBaseMesh(void);
	~CBaseMesh(void);

	inline MeshType GetType() { return m_Type;}

	void AddVert(Vertex vert);
	void AddTriangle(Triangle tri);
	
	void AppendVertsAndIndices(Vertex* vert, GLuint numVerts, Triangle* tri, GLuint numTris);

	void GenerateBuffers();
	void SendToGPU();
	void FlushVertsAndIndices();

	void UnLoad();

};

