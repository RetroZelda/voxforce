
#include "StdAfx.h"
#include "TextureManager.h"

// ensure it starts null
CTextureManager* CTextureManager::m_pInstance = NULL; 
int CTextureManager::m_nActiveInstances = 0;

CTextureManager::CTextureManager(void)
{
}

CTextureManager::~CTextureManager(void)
{
}

CTextureManager::CTextureManager(const CTextureManager& mm)
{
}

void CTextureManager::operator=(const CTextureManager mm)
{
}

CTextureManager* CTextureManager::GetInstance()
{
	if(m_pInstance == NULL)
	{
		assert(m_nActiveInstances == 0 && "m_nActiveInstances out of sync");
		m_pInstance = new CTextureManager();
	}
	m_nActiveInstances++;
	return m_pInstance;
}

void CTextureManager::ReleaseInstance()
{
	m_nActiveInstances--;

	// delete the instance if no one has a handle
	if(m_nActiveInstances == 0)
	{
		delete m_pInstance;
		m_pInstance = NULL;
	}
}