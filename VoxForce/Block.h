#pragma once
class CBlock
{
	friend class CChunk;
public:
    static float BLOCK_RENDER_SIZE;
	enum BlockType
	{
		BlockType_Default = 0,

		BlockType_Grass,
		BlockType_Dirt,
		BlockType_Water,
		BlockType_Stone,
		BlockType_Wood,
		BlockType_Sand,

		BlockType_NumTypes,
	};
private:
    bool m_bActive;

    BlockType m_blockType;
	glm::vec4 m_v4Color;
public:
    CBlock();
    ~CBlock();
	
	bool IsActive() { return m_bActive;}
	void SetActive(bool bActive) { m_bActive = bActive;}

	glm::vec4 GetColor() { return m_v4Color;}
	glm::vec4 SetColor(glm::vec4 v4Color) { m_v4Color = v4Color;}

	inline BlockType GetType() {return m_blockType;}

};
