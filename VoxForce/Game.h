#pragma once

#include "StdAfx.h"
#include "Renderer.h"
#include "Timer.h"

class CObjectManager;
class CMeshManager;
class CChunk;
class CPlayer;
class CGame
{
	CRenderer	m_Renderer;
	CTimer		m_GameTimer;

	CObjectManager* m_pObjectManager;
	CMeshManager* m_pMeshManager;

	// Window settings
	int window_width;
	int window_height;
	// HWND hWnd;
	// HINSTANCE hInstance;

	// Game
	bool m_bRunning;
	double m_dPrevTime;

public:
	CGame(void);
	~CGame(void);
	void Init(int width, int height, HWND hwnd, HINSTANCE hinstance);
	void Pause(bool bPause);
	void ResizeGame(int nWidth, int nHeight);
	void Update();
	void Shutdown();
	void Stop();

	inline bool IsRunning() { return m_bRunning;}
	inline int FPS() { return m_GameTimer.FPS();}
};

