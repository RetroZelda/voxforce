#pragma once
#include "StdAfx.h"
#include "RenderableObject.h"

class CChunkManager;
class CPlayer : public CRenderableObject
{
public:
	
	virtual void Init() override;
	virtual void Update(float fDeltaTime) override;
	virtual void Render(CRenderer* pRenderer) override;
	virtual void Shutdown() override;

	virtual ObjectType GetType() override { return PLAYER_OBJ;}

private:
	CChunkManager* m_pChunkManager;
public:
	CPlayer(void);
	~CPlayer(void);
};

