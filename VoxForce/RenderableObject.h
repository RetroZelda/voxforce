#pragma once
#include "StdAfx.h"
#include "BaseObject.h"

class CRenderer;
class CBaseMesh;
class CRenderableObject : public CBaseObject
{
protected:
	CBaseMesh* m_pMesh;
public:
	
	virtual void Init() override;
	virtual void Update(float fDeltaTime) override;
	virtual void Render(CRenderer* pRenderer);
	virtual void Shutdown() override;

	
	virtual ObjectType GetType() override { return RENDERABLE_OBJ;}
private:
	bool m_bShouldRender;
public:
	CRenderableObject(void);
	~CRenderableObject(void);

	inline void SetMesh(CBaseMesh* pMesh) { m_pMesh = pMesh;};
	inline CBaseMesh* GetMesh() { return m_pMesh;}

	inline bool ShouldRender() { return m_bShouldRender;}
	inline void SetShouldRender(bool bShouldRender) { m_bShouldRender = bShouldRender;}
};

