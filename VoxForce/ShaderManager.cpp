
#include "StdAfx.h"
#include "ShaderManager.h"


// ensure it starts null
CShaderManager* CShaderManager::m_pInstance = NULL;
int CShaderManager::m_nActiveInstances = 0;

CShaderManager::CShaderManager(void)
{
}


CShaderManager::~CShaderManager(void)
{
}

CShaderManager::CShaderManager(const CShaderManager& mm)
{
}

void CShaderManager::operator=(const CShaderManager mm)
{
}

CShaderManager* CShaderManager::GetInstance()
{
	if(m_pInstance == NULL)
	{
		assert(m_nActiveInstances == 0 && "m_nActiveInstances out of sync");
		m_pInstance = new CShaderManager();
	}
	m_nActiveInstances++;
	return m_pInstance;
}

void CShaderManager::ReleaseInstance()
{
	m_nActiveInstances--;

	// delete the instance if no one has a handle
	if(m_nActiveInstances == 0)
	{
		delete m_pInstance;
		m_pInstance = NULL;
	}
}

GLuint CShaderManager::LoadAndCompileShader(string file_path, GLenum shader_type)
{
	// open the vertex and Shader shader
	string shaderCode;
	fstream shaderStream(file_path.c_str(), ios_base::in | ios_base::binary);

	// ensure we have both streams open
	bool bShaderOpen = shaderStream.is_open();
	if(bShaderOpen == false)
	{
		printf("Unable to Open %s\n", file_path.c_str());
		shaderStream.clear();
		shaderStream.close();
		return 0;
	}

	// get the length of the file
	shaderStream.seekg(0, ios_base::end);
	int ShaderFileLength = (int)shaderStream.tellg();

	// size the string
	shaderCode.resize(ShaderFileLength);

	// read in everything
	shaderStream.seekg(0, ios_base::beg);
	shaderStream.read(&shaderCode[0], ShaderFileLength);

	// close the files
	shaderStream.clear();
	shaderStream.close();

	// Create the shaders
	GLuint ShaderID = glCreateShader(shader_type);

	GLint Result = GL_FALSE;
	int InfoLogLength;

	// Compile Shader
	printf("Compiling shader : %s\n", file_path.c_str());
	char const * ShaderSourcePointer = shaderCode.c_str();
	glShaderSource(ShaderID, 1, &ShaderSourcePointer , NULL);
	glCompileShader(ShaderID);

	// Check Shader for errors
	glGetShaderiv(ShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(ShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	vector<char> ShaderShaderErrorMessage(InfoLogLength);
	glGetShaderInfoLog(ShaderID, InfoLogLength, NULL, &ShaderShaderErrorMessage[0]);
	printf("%s\n", &ShaderShaderErrorMessage[0]);


	return ShaderID;

}

GLuint CShaderManager::LoadShadersFromFile(string vertex_file_path, string fragment_file_path, string geometry_file_path)
{
	GLuint VertexID = 0;
	GLuint FragmentID = 0;
	GLuint GeometryID = 0;

	// compile each shader type
	if(vertex_file_path != "")
	{
		VertexID = LoadAndCompileShader(vertex_file_path, GL_VERTEX_SHADER);
	}
	if(fragment_file_path != "")
	{
		FragmentID = LoadAndCompileShader(fragment_file_path, GL_FRAGMENT_SHADER);
	}
	if(geometry_file_path != "")
	{
		GeometryID = LoadAndCompileShader(geometry_file_path, GL_GEOMETRY_SHADER);
	}

	// Create the full program
	printf("Linking program... ");
	GLuint ProgramID = glCreateProgram();

	// attach the shaders we requested
	if(VertexID != 0)
	{
		glAttachShader(ProgramID, VertexID);
	}
	if(FragmentID != 0)
	{
		glAttachShader(ProgramID, FragmentID);
	}
	if(GeometryID != 0)
	{
		glAttachShader(ProgramID, GeometryID);
	}

	glLinkProgram(ProgramID);
	printf("Success!\n");

	// Check the program for errors
	GLint Result = GL_FALSE;
	int InfoLogLength;

	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	vector<char> ProgramErrorMessage( std::max(InfoLogLength, int(1)) );
	glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
	printf("%s\n", &ProgramErrorMessage[0]);

	// delete the shaders
	if(VertexID != 0)
	{
		glDeleteShader(VertexID);
	}
	if(FragmentID != 0)
	{
		glDeleteShader(FragmentID);
	}
	if(GeometryID != 0)
	{
		glDeleteShader(GeometryID);
	}

	// return the fresh compiled program
	return ProgramID;
}

GLuint CShaderManager::LoadShader(string szShaderName, GLuint shaderTypes)
{
	// check if the shader already exists
	map<string, GLuint>::iterator findResult = m_ShaderPrograms.find(szShaderName);
	if(findResult != m_ShaderPrograms.end())
	{
		// return the existing index
		return (*findResult).second;
	}

	// load in the shaders
	string szVertex = "";
	string szFragment = "";
	string szGeometry = "";
	if(shaderTypes & GL_VERTEX_SHADER)
	{
		szVertex = SHADER_DEFAULT_FOLDER + szShaderName + VERTEX_PROGRAM_DEFAULT_EXTENTION;
	}
	if(shaderTypes & GL_FRAGMENT_SHADER)
	{
		szFragment = SHADER_DEFAULT_FOLDER + szShaderName + FRAGMENT_PROGRAM_DEFAULT_EXTENTION;
	}
	if(shaderTypes & GL_GEOMETRY_SHADER)
	{
		szGeometry = SHADER_DEFAULT_FOLDER + szShaderName + GEOMETRY_PROGRAM_DEFAULT_EXTENTION;
	}
	GLuint ProgramID = LoadShadersFromFile(szVertex, szFragment, szGeometry);

	// ensure we have a valid program
	if(ProgramID > 0)
	{
		printf("Successfully loaded %s - ID = %d\n", szShaderName.c_str(), ProgramID);

		// bind the shader in the map
		m_ShaderPrograms[szShaderName] = ProgramID;
	}
	else
	{
		printf("Failed to load %s: %s\n", szShaderName.c_str(), glGetError());
	}

	return ProgramID;
}


GLuint CShaderManager::GetUniform(string szShaderName, string szUniform)
{
	GLuint programID = m_ShaderPrograms[szShaderName];
	GLuint uniformID = glGetUniformLocation(programID, szUniform.c_str());

	printf("%s uniform %s: %d\n", szShaderName.c_str(), szUniform.c_str(), uniformID);

	return uniformID;
}

void CShaderManager::UnloadShader(string szShaderName)
{
	map<string, GLuint>::iterator findResult = m_ShaderPrograms.find(szShaderName);
	if(findResult != m_ShaderPrograms.end())
	{
		m_ShaderPrograms.erase(findResult);
	}
}

void CShaderManager::BindProgram(string szShaderName)
{
	map<string, GLuint>::iterator findResult = m_ShaderPrograms.find(szShaderName);
	if(findResult != m_ShaderPrograms.end())
	{
		glUseProgram(findResult->second);
	}
}
