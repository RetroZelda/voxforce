#pragma once
#include "StdAfx.h"

class CChunk;
class CBlock;
class CRenderer;
class CRenderableObject;
class CCamera;
class CChunkManager
{
#pragma region _SINGLETON
private:
	// singleton: variables
	static CChunkManager* m_pInstance;
	static int m_nActiveInstances;

	// singleton: hide creationism
	CChunkManager(void);
	CChunkManager(const CChunkManager& mm);
	void operator=(const CChunkManager mm);
	~CChunkManager(void);
public:
	static CChunkManager* GetInstance();
	static void ReleaseInstance();
#pragma endregion
private:

	static const int CHUNKS_TO_LOAD_PER_FRAME = 1;
	
	int m_nWidth;
	int m_nHeight;
	int m_nDepth;

	// rendering/culling
	CCamera* m_pCamera;

	// chunk management
	CChunk*** m_WorldChunks; // every chunk
	vector<CChunk*> m_InitChunks; // chunks to initialize
	vector<CChunk*> m_BuildChunks; // chunks to rebuild
	vector<CChunk*> m_RenderChunks; // chunks to render

	// render callback
	void (*m_pRenderCallback)(CRenderableObject*, CRenderer*);

	
	void SetAdjacentChunks(CChunk* pChunk);

public:
	inline void SetRenderCallback(void (*pRenderFunc)(CRenderableObject*, CRenderer*)) { m_pRenderCallback = pRenderFunc;}


	void Init(int nWidth, int nHeight, int nDepth);
	void Update(float fDeltaTime);
	void Render(CRenderer* pRenderer);
	void Shutdown();

	void BuildChunkList();
	void InitChunkList();

	
	CChunk* GetChunk(glm::vec3 v3Pos);
	CChunk* GetBlock(glm::vec3 v3Pos, int* pIndexOut);
	CBlock* GetBlock(glm::vec3 v3Pos);
	CBlock* GetBlock(glm::vec3 v3Pos, CChunk*pChunk);
};

