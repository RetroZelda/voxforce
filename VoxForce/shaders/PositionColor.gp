#version 330
layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;


struct vpOutData
{
	vec4 vColor;
	vec2 vUV;

	vec3 surfaceNormal;
	vec3 lightDirection;
	vec3 eye;
	float distance;
};

in vpOutData vpOut[];
out vpOutData gpOut;

void main()
{
	// pass all the verts in each triangle through
	for(int vertexIndex = 0; vertexIndex < 3; vertexIndex++)
	{
		gl_Position = gl_in[vertexIndex].gl_Position;
		
		// pass the vertex program's data through
		gpOut = vpOut[vertexIndex];
		EmitVertex();
	}
	EndPrimitive();

}  