#version 330

struct vpOutData
{
	vec4 vColor;
	vec2 vUV;

	vec3 surfaceNormal;
	vec3 lightDirection;
	vec3 eye;
	float distance;
};

in vpOutData gpOut;
out vec3 color;

//uniform sampler2D diffuseTexture;

void main() 
{
	// TODO: make uniforms(from materials)
	vec4 diffuse = gpOut.vColor;
    vec4 ambient = vec4(0.0);
    vec4 specular= vec4(1.0, 1.0, 1.0, 1.0);
	float lightPower = 10;
    float shininess = 5 * lightPower;
	////////////////////////////////////// 
   vec4 spec = vec4(0.0);
 
    vec3 n = normalize(gpOut.surfaceNormal);
    vec3 l = normalize(gpOut.lightDirection);
    vec3 e = normalize(gpOut.eye);
 
    float intensity = max(dot(n,l), 0.0) * lightPower;
    if (intensity > 0.0) 
	{
        vec3 h = normalize(l + e);
        float intSpec = max(dot(h, n), 0.0);
        spec = specular * pow(intSpec, shininess);
    }
 
    color =  max((diffuse * intensity + spec) / (gpOut.distance) * lightPower, ambient).xyz;
    //color = diffuse.xyz;
}
