#include "StdAfx.h"
#include "BaseObject.h"


CBaseObject::CBaseObject(void)
{
	m_pParent = NULL;
	m_bDirty = true;

	memset(&m_v3Position, 0, sizeof(glm::vec3));
	memset(&m_qRotation, 0, sizeof(glm::vec3));
	m_qRotation.w = 1.0f;
	m_v3Scale.x = m_v3Scale.y = m_v3Scale.z = 1.0f;
}


CBaseObject::~CBaseObject(void)
{
}

void CBaseObject::MakeDirty()
{
	// we are dirty
	m_bDirty = true;

	// make our children dirty
	std::vector<CBaseObject*>::iterator childIter = m_pChildren.begin();
	for(; childIter != m_pChildren.end(); ++childIter)
	{
		(*childIter)->MakeDirty();
	}
}

void CBaseObject::BuildTransfromFromLocal()
{
	// get position
	m_v3Position.x = m_m4LocalMatrix[3].x;
	m_v3Position.y = m_m4LocalMatrix[3].y;
	m_v3Position.z = m_m4LocalMatrix[3].z;

	// get scale
	m_v3Scale.x = glm::length(m_m4LocalMatrix[0]);//.length();
	m_v3Scale.y = glm::length(m_m4LocalMatrix[1]);//.length();
	m_v3Scale.z = glm::length(m_m4LocalMatrix[2]);//.length();

	// get rotation
	m_qRotation = glm::quat(m_m4LocalMatrix);
}

void CBaseObject::SetParent(CBaseObject* pParent)
{
	// if we have a parent
	if(m_pParent != NULL)
	{
		// make the current world be the local
		const glm::mat4 &world = GetWorldMatrix();
		memcpy(&m_m4LocalMatrix, &world, sizeof(glm::mat4));

		// remove us from parents children
		vector<CBaseObject*>::iterator childIter = m_pParent->m_pChildren.begin();
		for(; childIter != m_pParent->m_pChildren.end(); ++childIter)
		{
			// remove us
			if(*childIter == this)
			{
				m_pParent->m_pChildren.erase(childIter);
				break;
			}
		}
	}

	// if we have a new parent
	if(pParent != NULL)
	{
		// make our local relative to the new parent(inv?)
		const glm::mat4 &world = pParent->GetWorldMatrix();
		m_m4LocalMatrix = glm::matrixCompMult(m_m4LocalMatrix, glm::inverse(world));

		// add us to its child list
		pParent->m_pChildren.push_back(this);

		BuildTransfromFromLocal();
	}

	MakeDirty();
	m_pParent = pParent;
}

const glm::mat4& CBaseObject::GetLocalMatrix()
{
	// update the matrix if dirty
	if(m_bDirty == true)
	{
		// get the components
		glm::mat4 pos = glm::translate(mat4(), m_v3Position);
		glm::mat4 rot = glm::toMat4(m_qRotation);
		glm::mat4 scl = glm::scale(mat4(), m_v3Scale);

		// combine them to our local
		//m_m4LocalMatrix = scl * rot * pos;
		m_m4LocalMatrix = pos * rot * scl;

		// not dirty
		m_bDirty = false;
	}

	return m_m4LocalMatrix;
}

const glm::mat4& CBaseObject::GetWorldMatrix()
{
	// become relative to our parent
	if(m_pParent != NULL)
	{
		glm::mat4 parentMat = m_pParent->GetWorldMatrix();
		glm::mat4 myLocal = GetLocalMatrix();
		m_m4WorldMatrix = parentMat * myLocal;
	}
	else
	{
		glm::mat4 myLocal = GetLocalMatrix();
		memcpy(&m_m4WorldMatrix, &myLocal, sizeof(glm::mat4));
	}

	return m_m4WorldMatrix;
}

const glm::vec3& CBaseObject::GetForward()
{
	const glm::mat4& local = GetLocalMatrix();
	glm::vec3& vec = (glm::vec3&)local[2];
	return vec;
}

const glm::vec3& CBaseObject::GetUp()
{
	const glm::mat4& local = GetLocalMatrix();
	glm::vec3& vec = (glm::vec3&)local[1];
	return vec;
}

const glm::vec3& CBaseObject::GetRight()
{
	const glm::mat4& local = GetLocalMatrix();
	glm::vec3& vec = (glm::vec3&)local[0];
	return vec;
}
const glm::vec3& CBaseObject::GetWorldForward()
{
	const glm::mat4& world = GetWorldMatrix();
	glm::vec3& vec = (glm::vec3&)world[2];
	return vec;
}

const glm::vec3& CBaseObject::GetWorldUp()
{
	const glm::mat4& world = GetWorldMatrix();
	glm::vec3& vec = (glm::vec3&)world[1];
	return vec;
}

const glm::vec3& CBaseObject::GetWorldRight()
{
	const glm::mat4& world = GetWorldMatrix();
	glm::vec3& vec = (glm::vec3&)world[0];
	return vec;
}

void CBaseObject::Translate(glm::vec3& trans)
{
	m_v3Position += (m_qRotation * trans);
	MakeDirty();
}

void CBaseObject::Translate(float fX, float fY, float fZ)
{
	glm::vec3 trans = vec3(fX, fY, fZ);
	Translate(trans);
}

void CBaseObject::Rotate(float fX, float fY, float fZ, float fAngle)
{
	glm::vec3 v3Axis = vec3(fX, fY, fZ);
	Rotate(v3Axis, fAngle);
}

void CBaseObject::Rotate(glm::vec3 v3Axis, float fAngle)
{
	m_qRotation = glm::rotate(m_qRotation, fAngle, v3Axis);
	MakeDirty();
}

void CBaseObject::LookAt(const glm::vec3& v3Target)
{
	glm::mat4 m4LookAt = glm::lookAt(m_v3Position, v3Target, glm::vec3(0.0f, 1.0f, 0.0f));
	m_qRotation = glm::toQuat(m4LookAt);
	MakeDirty();
}

void CBaseObject::Init()
{
}

void CBaseObject::Update(float fDeltaTime)
{
}

void CBaseObject::Shutdown()
{

}

glm::vec3 CBaseObject::GetWorldPosition()
{
	glm::vec3 pos = m_v3Position;

	// add to parent
	if(m_pParent != NULL)
	{
		pos = glm::vec3(m_pParent->GetWorldMatrix() * glm::vec4(pos, 1.0f));
	}

	return pos;
}
