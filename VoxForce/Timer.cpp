
#include "StdAfx.h"
#include "Timer.h"

CTimer::CTimer(void)
{
	m_bIsRunning = false;
	m_dElapsedTime = 0.0;

	QueryPerformanceFrequency( (LARGE_INTEGER*)&m_llFrequency );

	QueryPerformanceCounter( (LARGE_INTEGER*)&m_llStartTick );

	m_nFrameCount		= 0;	
	m_nFPS				= 0;			
	m_dFPSTimeStamp	= GetTickCount();
}

void CTimer::Start(void)
{
	if( !m_bIsRunning )
	{
		m_bIsRunning = true;
		Reset();
	}
}

void CTimer::Stop(void)
{
	if( m_bIsRunning )
	{
		m_bIsRunning = false;

		m_dElapsedTime = GetElapsedTime();
	}
}

void CTimer::Reset(void)
{
	m_dElapsedTime = 0.0;
	m_dFPSTimeStamp = 0;
	QueryPerformanceCounter( (LARGE_INTEGER*)&m_llStartTick );	
}

// TODO: THis functionality
void CTimer::Pause(bool bPause)
{
}

void CTimer::Update(void)
{
	if( m_bIsRunning )
	{
		// query the tick count
		LONGLONG	llStopTick;
		QueryPerformanceCounter( (LARGE_INTEGER*)&llStopTick );

		// convert to the time
		double dCurTime = (double)(llStopTick - m_llStartTick) / (double)m_llFrequency;
		m_dDeltaTime = dCurTime - m_dElapsedTime;
		m_dElapsedTime = dCurTime;

		
		// Count FPS
		m_nFrameCount++;

		// check if 1 second has passed
		if( m_dElapsedTime - m_dFPSTimeStamp > 1.0 )
		{
			// remember the frame count
			m_nFPS = m_nFrameCount;

			// reset the current frame count
			m_nFrameCount = 0;

			// get a new time stamp
			m_dFPSTimeStamp = m_dElapsedTime;
		}
	}
}

double CTimer::GetElapsedTime(void)
{
	return m_dElapsedTime;
}

double CTimer::GetDeltaTime(void)
{
	return m_dDeltaTime;
}

int CTimer::FPS(void)
{
	return m_nFPS;
}
