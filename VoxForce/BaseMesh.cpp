#include "StdAfx.h"
#include "BaseMesh.h"

CBaseMesh::CBaseMesh(void)
{
	m_VertexBuffer = -1;
	m_IndexBuffer = -1;
	m_InitFlags = 0;

	SetType(MESH_BASE);
}

CBaseMesh::~CBaseMesh(void)
{
}

void CBaseMesh::AppendVertsAndIndices(Vertex* vert, GLuint numVerts, Triangle* tri, GLuint numTris)
{
	// offset all the indicies then add them
	int nIndexOffset = m_Verts.size();
	for(unsigned int triIndex = 0; triIndex < numTris; ++triIndex)
	{
		tri[triIndex]._0 += nIndexOffset;
		tri[triIndex]._1 += nIndexOffset;
		tri[triIndex]._2 += nIndexOffset;

		AddTriangle(tri[triIndex]);
	}

	// add each vertex
	for(unsigned int vertIndex = 0; vertIndex < numVerts; ++vertIndex)
	{
		AddVert(vert[vertIndex]);
	}
}

void CBaseMesh::AddVert(Vertex vert)
{
	m_Verts.push_back(vert);
}

void CBaseMesh::AddTriangle(Triangle tri)
{
	m_Triangles.push_back(tri);
}

void CBaseMesh::GenerateBuffers()
{
	// create the vertex buffer
	if((m_InitFlags & VERTEX_INIT) == 0)
	{
		glGenBuffers(1, &m_VertexBuffer);
		m_InitFlags |= VERTEX_INIT;
	}

	// create the index buffer
	if((m_InitFlags & INDEX_INIT) == 0)
	{
		glGenBuffers(1, &m_IndexBuffer);
		m_InitFlags |= INDEX_INIT;
	}
}

void CBaseMesh::SendToGPU()
{
	GenerateBuffers();

	// bind verts
	glBindBuffer(GL_ARRAY_BUFFER, m_VertexBuffer);
	
	// set the alignment
	Vertex::EnableAttribArray();

	// give the verts to OpenGL
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * m_Verts.size(), NULL, GL_DYNAMIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex) * m_Verts.size(), &m_Verts[0]);

	Vertex::DisableAttribArray();

	// unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// bind indices
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IndexBuffer);

	// give the indices to OpenGL
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Triangle) * m_Triangles.size(), &m_Triangles[0], GL_DYNAMIC_DRAW);

	// unbind
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void CBaseMesh::FlushVertsAndIndices()
{
	m_Verts.clear();
	m_Triangles.clear();
}

void CBaseMesh::UnLoad()
{
	if ( m_IndexBuffer != 0 )
	{
		glDeleteBuffersARB( 1, &m_IndexBuffer );
		m_IndexBuffer = 0;
	}
	if ( m_VertexBuffer != 0 )
	{
		glDeleteBuffersARB( 1, &m_VertexBuffer );
		m_VertexBuffer = 0;
	}
	
}
