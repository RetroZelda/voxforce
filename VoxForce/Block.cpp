
#include "StdAfx.h"
#include "Block.h"

float CBlock::BLOCK_RENDER_SIZE = 1.0f;

CBlock::CBlock(void)
{
	static int colOrder = 0;
	int pickedCol = (colOrder++) % 3;

	switch(pickedCol)
	{
	case 0:
		{
			m_v4Color = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
		} break;
	case 1:
		{
			m_v4Color = glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
		} break;
	case 2:
		{
			m_v4Color = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
		} break;
	};
	m_bActive = true;
}


CBlock::~CBlock(void)
{
}
