#pragma once
#include "StdAfx.h"

class CBaseObject
{
public:
	enum ObjectType{BASE_OBJ, CAMERA_OBJ, RENDERABLE_OBJ, CHUNK_OBJ, PLAYER_OBJ};
private:
	// for creating a heirarchy
	CBaseObject* m_pParent;
	std::vector<CBaseObject*> m_pChildren;
	bool m_bDirty;

	// our transform information
	glm::vec3 m_v3Position;
	glm::vec3 m_v3Scale;
	glm::quat m_qRotation;
	glm::mat4 m_m4LocalMatrix;
	glm::mat4 m_m4WorldMatrix;

	void BuildTransfromFromLocal();

protected:
	void MakeDirty();
public:

	virtual void Init();
	virtual void Update(float fDeltaTime);
	virtual void Shutdown();

	virtual ObjectType GetType() { return BASE_OBJ;}

public:
	CBaseObject(void);
	~CBaseObject(void);

	void SetParent(CBaseObject* pParent);

	void Translate(glm::vec3& trans);
	void Translate(float fX, float fY, float fZ);

	void Rotate(float fX, float fY, float fZ, float fAngle);
	void Rotate(glm::vec3 v3Axis, float fAngle);
	
	void LookAt(const glm::vec3& v3Target);

	const glm::mat4& GetLocalMatrix();
	const glm::mat4& GetWorldMatrix();
	
	inline const glm::vec3& GetPosition() { return m_v3Position;}
	inline const glm::vec3& GetScale() { return m_v3Scale;}
	inline const glm::quat& GetRotation() { return m_qRotation;}
	
	glm::vec3 GetWorldPosition();
	
	const glm::vec3& GetForward();
	const glm::vec3& GetUp();
	const glm::vec3& GetRight();

	
	const glm::vec3& GetWorldForward();
	const glm::vec3& GetWorldUp();
	const glm::vec3& GetWorldRight();
};

