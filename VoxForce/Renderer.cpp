
#include "StdAfx.h"
#include "Renderer.h"
#include "ObjectManager.h"
#include "MeshManager.h"
#include "ShaderManager.h"

#include "BaseObject.h"
#include "RenderableObject.h"
#include "Camera.h"

CRenderer::CRenderer(void)
{
	m_ClearColor[0] = 0.0f;
	m_ClearColor[1] = 0.125f;
	m_ClearColor[2] = 0.3f;
	m_ClearColor[3] = 0.0f;

	m_pMeshManager = NULL;

	m_nWidth = 0;
	m_nHeight = 0;

	m_bInitialized = false;
	m_pCamera = NULL;
}


CRenderer::~CRenderer(void)
{
}


bool CRenderer::Init(HWND hWnd)
{
	glewExperimental = GL_TRUE; // Needed in core profile

	// get the window device context
	printf("Getting device context... ");
	m_hWnd = hWnd;
	m_hDeviceContext = GetDC(m_hWnd);

	// check if failed
	if(m_hDeviceContext == 0)
	{
		printf("Failed!\n");
		return false;
	}
	else
	{
		printf("Success!\n");
	}
	printf("Device Context %d on window %d\n", m_hWnd, m_hDeviceContext);

	// setup our pixel format
	PIXELFORMATDESCRIPTOR pfd;
	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 24;
	pfd.cDepthBits = 24;
	pfd.cStencilBits = 8;
	pfd.iLayerType = PFD_MAIN_PLANE;

	//__debugbreak();

	// set pixel format for the device context based on pixel format
	printf("Choosing Pixel Format... ");
	int pixelFormat = ChoosePixelFormat(m_hDeviceContext, &pfd);
	if(pixelFormat == 0)
	{
		printf("Failed!\n");
		return false;
	}
	printf("Success! (%d)\n", pixelFormat);

	// try and set the pixel format
	printf("Setting Pixel Format... ");
	BOOL bResult = SetPixelFormat(m_hDeviceContext, pixelFormat, &pfd);
	if(bResult == false)
	{
		printf("Failed\n");
		return false;
	}
	printf("Success!\n");


	// create the openGL context at v2.1
	HGLRC tempContext = wglCreateContext(m_hDeviceContext);
	wglMakeCurrent(m_hDeviceContext, tempContext);

	// enable glew
	GLenum error = glewInit();
	if(error != GLEW_OK)
	{
		printf("Unable to init glew!\n");
		return false;
	}


	// check and enable OpenGL 3.x extention
	GLboolean v3Supported = wglewIsSupported("WGL_ARB_create_context");
	if(v3Supported == 1)
	{
		// setup the OpenGL version/attributes
		int attributes[] =
		{
			WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
			WGL_CONTEXT_MINOR_VERSION_ARB, 2,
			WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
			0
		};
		printf("%d.%d ext. supported!\n", attributes[1], attributes[3]);

		m_hRenderContext = wglCreateContextAttribsARB(m_hDeviceContext, NULL, attributes);

		// remove the temp
		wglMakeCurrent(NULL, NULL);
		wglDeleteContext(tempContext);

		// set the 3.x to be the current
		wglMakeCurrent(m_hDeviceContext, m_hRenderContext);

		printf("Created new Context\n");

		GLenum error = glewInit();
		if(error != GLEW_OK)
		{
			printf("Unable to init glew for OpenGL %d.%d!\n", attributes[1], attributes[3]);
			return false;
		}
		else
		{
			printf("Glew Initialized!\n");
		}
	}
	else
	{
		printf("3.x not supported.  Defaulting to 2.1\n");

		// the 2.1 render context is what we are using
		m_hRenderContext = tempContext;
	}

	// dispaly OpenGL information
	int major = -1, minor = -1;
	glGetIntegerv(GL_MAJOR_VERSION, &major);
	glGetIntegerv(GL_MINOR_VERSION, &minor);
	printf("Using OpenGL: %d.%d\n", major, minor);

	// create/bind the VAO
	glGenVertexArrays(1, &m_VertexArrayID);
	glBindVertexArray(m_VertexArrayID);

	// get all of our singletons/managers
	m_pObjectManager = CObjectManager::GetInstance();
	m_pMeshManager = CMeshManager::GetInstance();
	m_pShaderManager = CShaderManager::GetInstance();

	// set our callbacks
	m_pObjectManager->SetRenderCallback(&CRenderer::MeshCallback);

	m_bInitialized = true;

	return true;
}


void CRenderer::SetupScene()
{
	// dont do anything if we are not initialized
	if(m_bInitialized == false)
	{
		printf("Cannot Setup the Scene... Not Initialized properly!\n");
		return;
	}

	// set the clear color
	glClearColor(m_ClearColor[0], m_ClearColor[1], m_ClearColor[2], m_ClearColor[3]);

	// enable extentions
	wglSwapIntervalEXT(false); // no vsync
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	// setup the projection matrix
	BuildProjection();

	// load the basic shader
	m_pShaderManager->LoadShader("PositionColor", GL_VERTEX_SHADER | GL_FRAGMENT_SHADER | GL_GEOMETRY_SHADER);

	// get teh uniforms
	m_MVP = m_pShaderManager->GetUniform("PositionColor", "MVP");
	m_Model = m_pShaderManager->GetUniform("PositionColor", "Model");
	m_View = m_pShaderManager->GetUniform("PositionColor", "View");
	m_Projection = m_pShaderManager->GetUniform("PositionColor", "Projection");
	m_ModelView = m_pShaderManager->GetUniform("PositionColor", "ModelView");
	m_NormalMat = m_pShaderManager->GetUniform("PositionColor", "NormalMatrix");
	m_LightPos = m_pShaderManager->GetUniform("PositionColor", "LightPos");

	// load a cube
	m_pMeshManager->AddCube();
	m_pMeshManager->AddPlane(100);
}


void CRenderer::Shutdown()
{

	// release all the singleton/managers
	CObjectManager::ReleaseInstance();
	CMeshManager::ReleaseInstance();
	CShaderManager::ReleaseInstance();

	// null them out
	m_pObjectManager = NULL;
	m_pMeshManager = NULL;
	m_pShaderManager = NULL;

	// remove Rendering context
	wglMakeCurrent(m_hDeviceContext, 0);
	wglDeleteContext(m_hRenderContext);

	// realease the device context
	ReleaseDC(m_hWnd, m_hDeviceContext);

	m_bInitialized = false;
}


void CRenderer::Bind()
{
}

void CRenderer::Unbind()
{
}

void CRenderer::BuildProjection()
{
	if(m_bInitialized == true)
	{
		if(m_pCamera != NULL)
		{
			m_pCamera->BuildProjection(60.0f, 0, 0, m_nWidth, m_nHeight, 1.0f, 1000.0f);
		}
	}
}

void CRenderer::ResizeWindow(UINT w, UINT h)
{
	m_nWidth = w;
	m_nHeight = h;
	BuildProjection();
}

void CRenderer::RenderBegin()
{
	// set the viewport and clear
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	// set to use the basic shader
	m_pShaderManager->BindProgram("PositionColor");
}

void CRenderer::RenderEnd()
{
	// render everything
	m_pObjectManager->Render(this);

	// present to the screen
	BOOL result = SwapBuffers(m_hDeviceContext);

	if(result == FALSE)
	{
		printf("SwapBuffers Failed\n");
	}

}

void CRenderer::MeshCallback(CRenderableObject* pObj, CRenderer* pRenderer)
{
	static CBaseObject testObj;
	CBaseMesh* pMesh = pObj->GetMesh();

	// calculate our MVP
	glm::mat4 m4ModelMatrix = pObj->GetWorldMatrix();
	glm::mat4 m4ViewMatrix  = pRenderer->m_pCamera->GetView();
	glm::mat4 m4ProjMatrix  = pRenderer->m_pCamera->GetProjection();
	glm::mat4 m4MV			= m4ViewMatrix * m4ModelMatrix;
	glm::mat4 m4MVP			= m4ProjMatrix * m4MV;
	glm::mat3 m4NormalMat	= glm::transpose(glm::inverse(glm::mat3(m4MV)));
	glm::vec3 CamPos		= pRenderer->m_pCamera->GetWorldPosition();

	//printf("cam pos: (%f, %f, %f)\n", CamPos.x, CamPos.y, CamPos.z);

	// set uniforms
	glUniformMatrix4fv(pRenderer->m_MVP, 1, GL_FALSE, (GLfloat*)&m4MVP);
	glUniformMatrix4fv(pRenderer->m_Model, 1, GL_FALSE, (GLfloat*)&m4ModelMatrix);
	glUniformMatrix4fv(pRenderer->m_View, 1, GL_FALSE, (GLfloat*)&m4ViewMatrix);
	glUniformMatrix4fv(pRenderer->m_Projection, 1, GL_FALSE, (GLfloat*)&m4ProjMatrix);
	glUniformMatrix4fv(pRenderer->m_ModelView, 1, GL_FALSE, (GLfloat*)&m4ProjMatrix);
	glUniformMatrix3fv(pRenderer->m_NormalMat, 1, GL_FALSE, (GLfloat*)&m4NormalMat);
	glUniform3fv(pRenderer->m_LightPos, 1, (GLfloat*)&CamPos[0]);


	// bind our buffers
	glBindBuffer(GL_ARRAY_BUFFER, pMesh->m_VertexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pMesh->m_IndexBuffer);


	// enable our vert state
	//Vertex::EnableClientState();
	//Vertex::SetVertexPointers();
	Vertex::EnableAttribArray();

	// render
	glDrawElements(GL_TRIANGLES, pMesh->m_Triangles.size() * 3, GL_UNSIGNED_SHORT, BUFFER_OFFSET(0));//(GLvoid*)pMesh->m_pIndices);

	// disable our vert state
	Vertex::DisableAttribArray();
	//Vertex::DisableClientState();

}

