#include "StdAfx.h"
#include "RenderableObject.h"
#include "Renderer.h"
#include "BaseMesh.h"

CRenderableObject::CRenderableObject(void)
{
	m_pMesh = NULL;
	m_bShouldRender = false;
}


CRenderableObject::~CRenderableObject(void)
{
}


void CRenderableObject::Init()
{
	m_bShouldRender = true;
}

void CRenderableObject::Update(float fDeltaTime)
{
}

void CRenderableObject::Render(CRenderer* pRenderer)
{
}

void CRenderableObject::Shutdown()
{
}
   