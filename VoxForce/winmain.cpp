
#include "StdAfx.h"

// VLD
//#include <vld.h>

// for console
#include <io.h>
#include <fcntl.h>

// main game
#include "Game.h"

// Globals
const char* const szWindowClass = "VoxFOrce";
const char* const szTitle = "VoxForce - VoxelEngine by RetroZelda";

CGame* g_pGame;

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	//	This is the main message handler of the system.
	PAINTSTRUCT	ps;			//	Used in WM_PAINT.
	HDC			hdc;		//	Handle to a device context.

	//	What is the message
	switch(message)
	{
		//	To skip ALT pop up menu (system menu)
		case WM_SYSKEYUP:
		case WM_SYSCHAR:
			return(0);
		break;
		case WM_SIZE:
		{
			// resize the render window
			int nWidth = LOWORD(lParam);
			int nHeight = HIWORD(lParam);
			g_pGame->ResizeGame(nWidth, nHeight);
		}
		break;

		//	Handle ALT+F4
		case WM_CLOSE:
		case  WM_QUIT:
		{
			// Sends us a WM_DESTROY
			g_pGame->Stop();
		}
		break;

		//	and lose/gain focus
		case WM_ACTIVATE:
		{
			//	gaining focus
			if (LOWORD(wParam) != WA_INACTIVE)
			{
				// unpause game code here
				g_pGame->Pause(false);
			}
			else // losing focus
			{
				// pause game code here
				g_pGame->Pause(true);
			}
		}
		break;

		case WM_CREATE:
		{
			//	Do initialization here
			return(0);
		}
		break;

		case WM_PAINT:
		{
			//	Start painting
			hdc = BeginPaint(hWnd,&ps);

			//	End painting
			EndPaint(hWnd,&ps);
			return(0);
		}
		break;

		case WM_DESTROY:
		{
			//	Kill the application
			PostQuitMessage(0);
			return(0);
		}
		break;

		default:
		break;
	}

	//	Process any messages that we didn't take care of
	return (DefWindowProc(hWnd, message, wParam, lParam));
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	int width = 800;
	int height = 600;

	g_pGame = new CGame();


	// create a console window
	AllocConsole();
	HANDLE handle_out = GetStdHandle(STD_OUTPUT_HANDLE);
    int hCrt = _open_osfhandle((long) handle_out, _O_TEXT);
    FILE* hf_out = _fdopen(hCrt, "w");
    setvbuf(hf_out, NULL, _IONBF, 1);
    *stdout = *hf_out;

    HANDLE handle_in = GetStdHandle(STD_INPUT_HANDLE);
    hCrt = _open_osfhandle((long) handle_in, _O_TEXT);
    FILE* hf_in = _fdopen(hCrt, "r");
    setvbuf(hf_in, NULL, _IONBF, 128);
    *stdin = *hf_in;

	// create the window details
	// TODO: uncomment these and make them work
	WNDCLASSEX wcex;
	wcex.cbSize			= sizeof(WNDCLASSEX);
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, IDI_APPLICATION);
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = NULL;
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, IDI_APPLICATION);


	// register window class
	BOOL registerClassResult = RegisterClassEx(&wcex);
	if(registerClassResult == FALSE)
	{
		MessageBox(NULL, "Failed to register windows class!", "Error!", MB_OK);
		return GetLastError();
	}

	// create the window
	DWORD windowStyle = WS_OVERLAPPEDWINDOW;
	HWND hWnd = CreateWindow(szWindowClass, szTitle, windowStyle, CW_USEDEFAULT, CW_USEDEFAULT, width, height, NULL, NULL, hInstance, NULL);
	if(!hWnd)
	{
		MessageBox(NULL, "Failed to create window handle!", "Error", MB_OK);
		return GetLastError();
	}


	//system("pause");

	// init the game
	g_pGame->Init(width, height, hWnd, hInstance);

	printf("Opening Window!\n");
	// display the window
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// message loop
	MSG msg;
	bool running = true;
	char TitleBuffer[255] = {0};
	int nFPS = 0;
	while(running)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			//	Test if this is a quit
			if (msg.message == WM_QUIT)
			{
				running = false;
			}

			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		// update
		g_pGame->Update();

		// show fps as title
		int curFPS = g_pGame->FPS();
		if(curFPS != nFPS)
        {
            //TODO: sprintf_s(TitleBuffer, 255, "%s - FPS: %d", szTitle, nFPS);
            nFPS = curFPS;
            sprintf(TitleBuffer, "%s - FPS: %d", szTitle, nFPS);
            SetWindowText(hWnd, TitleBuffer);
        }

		// for now
		//Sleep(1);
	}

	// shutdown the game
	g_pGame->Shutdown();
	delete g_pGame;

	// close the window
	DestroyWindow(hWnd);

	// require input before closing the console window
	//system("pause");

	return 0;
}
