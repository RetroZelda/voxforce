#pragma once
#include "StdAfx.h"
#include "RenderableObject.h"
#include "Block.h"

class CRenderer;
class CChunk : public CRenderableObject
{
public:
    static const int CHUNK_SIZE = 8;
	enum ChunkStatus { CHUNK_CREATED, CHUNK_INITIALIZED, CHUNK_LOADING, CHUNK_BUILDING, CHUNK_READY, CHUNK_NEED_REBUILT, CHUNK_REBUILD_QUEUED};
private:

    // The blocks data
    CBlock*** m_pBlocks;

	// our location withing the world
	int m_posX;
	int m_posY;
	int m_posZ;

	// adjacent chunks
	CChunk* m_pLeft;
	CChunk* m_pRight;
	CChunk* m_pFront;
	CChunk* m_pBack;
	CChunk* m_pTop;
	CChunk* m_pBottom;
	
	// our status
	ChunkStatus m_Status;
	bool m_bBlocksToRender;

	void CreateCube(float posX, float posY, float posZ, glm::vec4 color);
	bool CheckAdjacentBlocks(int X, int Y, int Z, int MaxLimit);
public:
    CChunk();
    ~CChunk();
	
	void BuildMesh();
	void SetStatus(ChunkStatus status);

	void SetAdjacent(CChunk* pLeft,		CChunk* pRight, 
					 CChunk* pFront,	CChunk* pBack,
					 CChunk* pTop,		CChunk* pBottom);

	void SetMyPosition(int posX, int posY, int posZ);
	bool DisableBlock(int X, int Y, int Z);

	inline ChunkStatus GetStatus() { return m_Status;}
	inline bool ShouldRender() { return m_bBlocksToRender;}
	
	virtual void Init() override;
	virtual void Update(float fDeltaTime) override;
	virtual void Render(CRenderer* pRenderer);
	virtual void Shutdown() override;

	
	inline int GetPosX() { return m_posX;}
	inline int GetPosY() { return m_posY;}
	inline int GetPosZ() { return m_posZ;}

	virtual ObjectType GetType() override { return CHUNK_OBJ;}
	inline CBlock& GetBlock(int X, int Y, int Z) { return m_pBlocks[X][Y][Z];}
	
};
