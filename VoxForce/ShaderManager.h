#pragma once

#include "StdAfx.h"
#include <GL/glew.h>
#include <GL/wglew.h>


// setting defines
#define SHADER_EXTENTION_LENGTH 3
#define SHADER_DEFAULT_FOLDER "./shaders/"
#define VERTEX_PROGRAM_DEFAULT_EXTENTION ".vp"
#define FRAGMENT_PROGRAM_DEFAULT_EXTENTION ".fp"
#define GEOMETRY_PROGRAM_DEFAULT_EXTENTION ".gp"

class CShaderManager
{
#pragma region _SINGLETON
private:
	// singleton: variables
	static CShaderManager* m_pInstance;
	static int m_nActiveInstances;
	
	// singleton: hide creationism
	CShaderManager(void);
	CShaderManager(const CShaderManager& mm);
	void operator=(const CShaderManager mm);
	~CShaderManager(void);
public:
	static CShaderManager* GetInstance();
	static void ReleaseInstance();
#pragma endregion
	
private:
	
	std::map<string, GLuint> m_ShaderPrograms;

	GLuint LoadAndCompileShader(string file_path, GLenum shader_type);
	GLuint LoadShadersFromFile(string vertex_file_path, string fragment_file_path, string geometry_file_path);

public:
	GLuint LoadShader(string szShaderName, GLuint shaderTypes);
	GLuint GetUniform(string szShaderName, string szUniform);
	void UnloadShader(string szShaderName);
	void BindProgram(string szShaderName);
};

