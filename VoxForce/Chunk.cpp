
#include "StdAfx.h"
#include "Block.h"
#include "Chunk.h"
#include "BaseMesh.h"


CChunk::CChunk()
{
	m_posX = 0;
	m_posY = 0;
	m_posZ = 0;
	m_pBlocks = NULL;
	m_Status = CHUNK_CREATED;

	m_pLeft = NULL;
	m_pRight = NULL;
	m_pFront = NULL;
	m_pBack = NULL;
	m_pTop = NULL;
	m_pBottom = NULL;
}


CChunk::~CChunk(void)
{
}

void CChunk::Update(float fDeltaTime)
{
	if(GetAsyncKeyState('O'))
	{
		//CBlock::BLOCK_RENDER_SIZE += 0.01f;
		m_posY++;
		BuildMesh();
	}
	else if(GetAsyncKeyState('P'))
	{
		//CBlock::BLOCK_RENDER_SIZE -= 0.01f;
		m_posY--;
		BuildMesh();
	}

	static bool prevSpace = false;
	bool curSpace = GetAsyncKeyState(VK_SPACE) != 0;

	// if holding space, remove random voxels
	if(curSpace)
	{
		int max = CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE;
		int chosenX = rand() % CHUNK_SIZE;
		int chosenY = rand() % CHUNK_SIZE;
		int chosenZ = rand() % CHUNK_SIZE;

		CBlock& block = m_pBlocks[chosenX][chosenY][chosenZ];
		block.SetActive(false);
	}

	// when space released, rebuild
	if(!curSpace && prevSpace)
	{
		BuildMesh();
	}
	prevSpace = curSpace;
}

void CChunk::Render(CRenderer* pRenderer)
{
}


void CChunk::SetAdjacent(CChunk* pLeft,		CChunk* pRight, 
	/**********************/ CChunk* pFront,	CChunk* pBack,
	/**********************/ CChunk* pTop,		CChunk* pBottom)
{
	m_pLeft		= pLeft;
	m_pRight	= pRight;
	m_pFront	= pFront;
	m_pBack		= pBack;
	m_pTop		= pTop;
	m_pBottom	= pBottom;
}

void CChunk::SetMyPosition(int posX, int posY, int posZ)
{
	m_posX = posX;
	m_posY = posY;
	m_posZ = posZ;
}

void CChunk::Init()
{
	// Create the X block
	m_pBlocks = new CBlock**[CHUNK_SIZE];
	for(int i = 0; i < CHUNK_SIZE; i++)
	{
		// crete the Y blocks
		m_pBlocks[i] = new CBlock*[CHUNK_SIZE];

		for(int j = 0; j < CHUNK_SIZE; j++)
		{
			// create the Z blocks
			m_pBlocks[i][j] = new CBlock[CHUNK_SIZE];
		}
	}

	// create our mesh
	m_pMesh = new CBaseMesh();
	SetStatus(CHUNK_INITIALIZED);
}

void CChunk::SetStatus(ChunkStatus status)
{
	m_Status = status;
}

void CChunk::BuildMesh()
{  
	// NOTE: Setting status at beginning and end for future threadding need
	SetStatus(CHUNK_BUILDING);

	int nExpecting = CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE;
	int nBuilt = 0;

	m_pMesh->FlushVertsAndIndices();
	for (int x = 0; x < CHUNK_SIZE; x++)
	{
		for (int y = 0; y < CHUNK_SIZE; y++)
		{
			for (int z = 0; z < CHUNK_SIZE; z++)
			{
				CBlock& pCurBlock = m_pBlocks[x][y][z];

				// check adjacent blocks to see if we are fully hidden
				int nMaxLimit = CHUNK_SIZE - 1;
				bool bDraw = pCurBlock.IsActive();

				if(bDraw == true)
				{
					// check if an adjacent block is hidden
					bool bShouldShow = CheckAdjacentBlocks(x, y, z, nMaxLimit);
					if(bShouldShow == true)
					{
						// yes, we can build
						nBuilt++;
						CreateCube((float)x, (float)y, (float)z, pCurBlock.GetColor());	
					}	
				}
			}
		}
	}

	/*
	printf("My Position: (%d, %d, %d)\n", m_posX, m_posY, m_posZ);
	printf("Blocks Built %d / %d | %f\n", nBuilt, nExpecting, 100.0f * (float(nBuilt)/float(nExpecting)));
	printf("Built Verts: %d | Built Triangles: %d\n", m_pMesh->m_Verts.size(), m_pMesh->m_Triangles.size());
	*/

	// dont render if we have 0 blocks
	if(nBuilt > 0)
	{
		m_bBlocksToRender = true;
		m_pMesh->SendToGPU();	
	}
	else
	{
		m_bBlocksToRender = false;
	}

	SetStatus(CHUNK_READY);
}


bool CChunk::DisableBlock(int X, int Y, int Z)
{
	// ensure we are not already disabled
	CBlock& block = GetBlock(X, Y, Z);
	if(block.IsActive() == false)
	{
		return false;
	}

	// disable the block
 	block.SetActive(false);

	// set our status cuz we need to build
	SetStatus(CHUNK_NEED_REBUILT);

	// if we are on an edge, we need to redraw our neightbor
	int maxLimit = CHUNK_SIZE - 1;
	if(X == 0 )
	{
		if(m_pLeft != NULL && m_pLeft->GetStatus() == CHUNK_READY)
		{
			if(m_pLeft->GetBlock(maxLimit, Y, Z).IsActive() == true)
			m_pLeft->SetStatus(CHUNK_NEED_REBUILT);
		}
	}

	if(Y == 0)
	{
		if(m_pBottom != NULL && m_pBottom->GetStatus() == CHUNK_READY)
		{
			if(m_pBottom->GetBlock(X, maxLimit, Z).IsActive() == true)
			m_pBottom->SetStatus(CHUNK_NEED_REBUILT);
		}
	}

	if(Z == 0)
	{
		if(m_pFront != NULL && m_pFront->GetStatus() == CHUNK_READY)
		{
			if(m_pFront->GetBlock(X, Y, maxLimit).IsActive() == true)
			m_pFront->SetStatus(CHUNK_NEED_REBUILT);
		}
	}

	if(X == maxLimit)
	{
		if(m_pRight != NULL && m_pRight->GetStatus() == CHUNK_READY)
		{
			if(m_pRight->GetBlock(0, Y, Z).IsActive() == true)
			m_pRight->SetStatus(CHUNK_NEED_REBUILT);
		}
	}

	if(Y == maxLimit)
	{
		if(m_pTop != NULL && m_pTop->GetStatus() == CHUNK_READY)
		{
			if(m_pTop->GetBlock(X, 0, Z).IsActive() == true)
			m_pTop->SetStatus(CHUNK_NEED_REBUILT);
		}
	}

	if(Z == maxLimit )
	{
		if(m_pBack != NULL && m_pBack->GetStatus() == CHUNK_READY)
		{
			if(m_pBack->GetBlock(X, Y, 0).IsActive() == true)
			m_pBack->SetStatus(CHUNK_NEED_REBUILT);
		}
	}

	return true;
}

bool CChunk::CheckAdjacentBlocks(int X, int Y, int Z, int MaxLimit)
{
	// get left
	int nAdjacentIndex = X - 1;
	CBlock* pAdjacent;

	// check the left adjacent chunk
	if(nAdjacentIndex == -1)
	{
		// check if the rightmost block is active
		if(m_pLeft != NULL)
		{
			pAdjacent = &m_pLeft->GetBlock(MaxLimit, Y, Z);
			if(pAdjacent->IsActive() == false)
			{
				// not active, we will have to be shown
				return true;
			}
		}
		else
		{
			// no left adjacent chunk
			return true;
		}
	}
	else
	{
		pAdjacent = &GetBlock(nAdjacentIndex, Y, Z);
		if(pAdjacent->IsActive() == false)
		{
			// not active, we will have to be shown
			return true;
		}
	}

	// get right
	nAdjacentIndex = X + 1;

	// check the right adjacent chunk
	if(nAdjacentIndex == CHUNK_SIZE)
	{
		// check if the rightmost block is active
		if(m_pRight != NULL)
		{
			pAdjacent = &m_pRight->GetBlock(0, Y, Z);
			if(pAdjacent->IsActive() == false)
			{
				// not active, we will have to be shown
				return true;
			}
		}
		else
		{
			// no left adjacent chunk
			return true;
		}
	}
	else
	{
		pAdjacent = &GetBlock(nAdjacentIndex, Y, Z);
		if(pAdjacent->IsActive() == false)
		{
			// not active, we will have to be shown
			return true;
		}
	}

	// get top
	nAdjacentIndex = Y + 1;

	// check the right adjacent chunk
	if(nAdjacentIndex == CHUNK_SIZE)
	{
		// check if the rightmost block is active
		if(m_pTop != NULL)
		{
			pAdjacent = &m_pTop->GetBlock(X, 0, Z);
			if(pAdjacent->IsActive() == false)
			{
				// not active, we will have to be shown
				return true;
			}
		}
		else
		{
			// no left adjacent chunk
			return true;
		}
	}
	else
	{
		pAdjacent = &GetBlock(X, nAdjacentIndex, Z);
		if(pAdjacent->IsActive() == false)
		{
			// not active, we will have to be shown
			return true;
		}
	}

	// get bottom
	nAdjacentIndex = Y - 1;

	// check the right adjacent chunk
	if(nAdjacentIndex == -1)
	{
		// check if the rightmost block is active
		if(m_pBottom != NULL)
		{
			pAdjacent = &m_pBottom->GetBlock(X, MaxLimit, Z);
			if(pAdjacent->IsActive() == false)
			{
				// not active, we will have to be shown
				return true;
			}
		}
		else
		{
			// no left adjacent chunk
			return true;
		}
	}
	else
	{
		pAdjacent = &GetBlock(X, nAdjacentIndex, Z);
		if(pAdjacent->IsActive() == false)
		{
			// not active, we will have to be shown
			return true;
		}
	}

	// get front
	nAdjacentIndex = Z - 1;

	// check the right adjacent chunk
	if(nAdjacentIndex == -1)
	{
		// check if the rightmost block is active
		if(m_pFront != NULL)
		{
			pAdjacent = &m_pFront->GetBlock(X, Y, MaxLimit);
			if(pAdjacent->IsActive() == false)
			{
				// not active, we will have to be shown
				return true;
			}
		}
		else
		{
			// no left adjacent chunk
			return true;
		}
	}
	else
	{
		pAdjacent = &GetBlock(X, Y, nAdjacentIndex);
		if(pAdjacent->IsActive() == false)
		{
			// not active, we will have to be shown
			return true;
		}
	}

	// get back
	nAdjacentIndex = Z + 1;

	// check the right adjacent chunk
	if(nAdjacentIndex == CHUNK_SIZE)
	{
		// check if the rightmost block is active
		if(m_pBack != NULL)
		{
			pAdjacent = &m_pBack->GetBlock(X, Y, 0);
			if(pAdjacent->IsActive() == false)
			{
				// not active, we will have to be shown
				return true;
			}
		}
		else
		{
			// no left adjacent chunk
			return true;
		}
	}
	else
	{
		pAdjacent = &GetBlock(X, Y, nAdjacentIndex);
		if(pAdjacent->IsActive() == false)
		{
			// not active, we will have to be shown
			return true;
		}
	}

	// we have all adjacents enables
	return false;
}

void CChunk::CreateCube(float posX, float posY, float posZ, glm::vec4 color)
{
	// the chunk offset(width, height, depth)
	float fChunkOffsetX = m_posX * CBlock::BLOCK_RENDER_SIZE * CHUNK_SIZE;
	float fChunkOffsetY = m_posY * CBlock::BLOCK_RENDER_SIZE * CHUNK_SIZE;
	float fChunkOffsetZ = m_posZ * CBlock::BLOCK_RENDER_SIZE * CHUNK_SIZE;

	// all the vert positions
	float fBlockOffset = CBlock::BLOCK_RENDER_SIZE / 2.0f;
	posX *= CBlock::BLOCK_RENDER_SIZE;
	posY *= CBlock::BLOCK_RENDER_SIZE;
	posZ *= CBlock::BLOCK_RENDER_SIZE;
	glm::vec3 p1(fChunkOffsetX + (posX - fBlockOffset), fChunkOffsetY + (posY - fBlockOffset), fChunkOffsetZ + (posZ + fBlockOffset));
	glm::vec3 p2(fChunkOffsetX + (posX + fBlockOffset), fChunkOffsetY + (posY - fBlockOffset), fChunkOffsetZ + (posZ + fBlockOffset));
	glm::vec3 p3(fChunkOffsetX + (posX + fBlockOffset), fChunkOffsetY + (posY + fBlockOffset), fChunkOffsetZ + (posZ + fBlockOffset));
	glm::vec3 p4(fChunkOffsetX + (posX - fBlockOffset), fChunkOffsetY + (posY + fBlockOffset), fChunkOffsetZ + (posZ + fBlockOffset));
	glm::vec3 p5(fChunkOffsetX + (posX + fBlockOffset), fChunkOffsetY + (posY - fBlockOffset), fChunkOffsetZ + (posZ - fBlockOffset));
	glm::vec3 p6(fChunkOffsetX + (posX - fBlockOffset), fChunkOffsetY + (posY - fBlockOffset), fChunkOffsetZ + (posZ - fBlockOffset));
	glm::vec3 p7(fChunkOffsetX + (posX - fBlockOffset), fChunkOffsetY + (posY + fBlockOffset), fChunkOffsetZ + (posZ - fBlockOffset));
	glm::vec3 p8(fChunkOffsetX + (posX + fBlockOffset), fChunkOffsetY + (posY + fBlockOffset), fChunkOffsetZ + (posZ - fBlockOffset));

	// our vertices
	Vertex verts[24];
	glm::vec3 norm;
	glm::vec3 norm0 = glm::vec3(0);

	// Front
	norm = glm::vec3(0.0f, 0.0f, 1.0f);
	verts[0] = Vertex::MakeVertex(p1, vec2(0, 0), norm, color);
	verts[1] = Vertex::MakeVertex(p2, vec2(0, 0), norm, color);
	verts[2] = Vertex::MakeVertex(p3, vec2(0, 0), norm, color);
	verts[3] = Vertex::MakeVertex(p4, vec2(0, 0), norm, color);

	// Back
	norm = glm::vec3(0.0f, 0.0f, -1.0f);
	verts[4] = Vertex::MakeVertex(p5, vec2(0, 0), norm, color);
	verts[5] = Vertex::MakeVertex(p6, vec2(0, 0), norm, color);
	verts[6] = Vertex::MakeVertex(p7, vec2(0, 0), norm, color);
	verts[7] = Vertex::MakeVertex(p8, vec2(0, 0), norm, color);

	// Right
	norm = glm::vec3(1.0f, 0.0f, 0.0f);
	verts[8] = Vertex::MakeVertex(p2, vec2(0, 0), norm, color);
	verts[9] = Vertex::MakeVertex(p5, vec2(0, 0), norm, color);
	verts[10] = Vertex::MakeVertex(p8, vec2(0, 0), norm, color);
	verts[11] = Vertex::MakeVertex(p3, vec2(0, 0), norm, color);

	// left
	norm = glm::vec3(-1.0f, 0.0f, 0.0f);
	verts[12] = Vertex::MakeVertex(p6, vec2(0, 0), norm, color);
	verts[13] = Vertex::MakeVertex(p1, vec2(0, 0), norm, color);
	verts[14] = Vertex::MakeVertex(p4, vec2(0, 0), norm, color);
	verts[15] = Vertex::MakeVertex(p7, vec2(0, 0), norm, color);	

	// Top
	norm = glm::vec3(0.0f, 1.0f, 0.0f);
	verts[16] = Vertex::MakeVertex(p4, vec2(0, 0), norm, color);
	verts[17] = Vertex::MakeVertex(p3, vec2(0, 0), norm, color);
	verts[18] = Vertex::MakeVertex(p8, vec2(0, 0), norm, color);
	verts[19] = Vertex::MakeVertex(p7, vec2(0, 0), norm, color);

	// Bottom
	norm = glm::vec3(0.0f, -1.0f, 0.0f);
	verts[20] = Vertex::MakeVertex(p6, vec2(0, 0), norm, color);
	verts[21] = Vertex::MakeVertex(p5, vec2(0, 0), norm, color);
	verts[22] = Vertex::MakeVertex(p2, vec2(0, 0), norm, color);
	verts[23] = Vertex::MakeVertex(p1, vec2(0, 0), norm, color);

	// our triangles
	GLushort triangles[12][3] =
	{
		{0, 1, 2},
		{0, 2, 3},
		{4, 5, 6},
		{4, 6, 7},
		{8, 9, 10},
		{8, 10, 11},
		{12, 13, 14},
		{12, 14, 15},
		{16, 17, 18},
		{16, 18, 19},
		{20, 21, 22},
		{20, 22, 23},
	};

	m_pMesh->AppendVertsAndIndices(verts, 24, (Triangle*)&triangles[0][0], 12);
}

void CChunk::Shutdown()
{
	// Delete the blocks
	for (int i = 0; i < CHUNK_SIZE; ++i)
	{
		for (int j = 0; j < CHUNK_SIZE; ++j)
		{
			// delete the Z blocks
			delete [] m_pBlocks[i][j];
		}

		// delete the Y blocks
		delete [] m_pBlocks[i];
	}
	// delete hte X blocks
	delete [] m_pBlocks;

	// delete the mesh
	m_pMesh->UnLoad();
	delete m_pMesh;
	m_pMesh = NULL;
}
